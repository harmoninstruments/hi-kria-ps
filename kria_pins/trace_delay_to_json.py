# Copyright 2022 Harmon Instruments, LLC
# MIT license
#
# generate a JSON representation of average die bump to SOM connector
# pin delay in picoseconds
#
# keys are the connector pin name

import json

package_lengths = {} # keys are package ball
package_mio = {} # keys are MIO number integer

# generate with Vivado 'write_csv' command
with open('packagedelays.csv') as f:
    for l in f:
        if len(l) == 0: # blank lines
            continue
        if l[0] == '#': # comments
            continue
        if 'IO Bank,Pin Number' in l: # field descriptions
            continue
        l = l.split(',')
        try:
            pin = l[1]
            # the Vivado export delays are in picoseconds, min, max
            delay = 0.5*(float(l[4])+float(l[5]))
            package_lengths[pin] = delay
            if "MIO" in l[3]:
                package_mio[int(l[3][6:-4])] = delay # l[3] is PS_MIOnnn_500
        except:
            pass

def get_lengths(conn):
    som_lengths = {}
    mio_pins = {}
    delay = {}
    # generated with parse_xdc.py
    with open(f"som{conn[-1]}.json") as f:
        conn_to_ball = json.load(f)
    # not included in repo due to copyright, found in Xilinx XTP688
    with open('../kria_pins/Kria_K26_Trace_Delay.csv') as f:
        for l in f:
            d = l.split(',')
            if not conn in d[0]:
                continue
            name = f"{d[0][4]}{int(d[0][5:]):02d}"
            if name[1] == '0': # no leading zeros in conn_to_ball A01 to A1
                sname = name[0]+name[2:]
            else:
                sname = name
            # nanoseconds
            som_delay = round((float(d[2]) + float(d[3])) * 0.5 * 1000.0, 3)
            som_lengths[name] = som_delay
            package_delay = 0
            if 'MIO' in d[1]:
                mionum = int(d[1].split('_')[0][3:])
                package_delay = package_mio[mionum]
                print("found MIO", name, mionum, som_delay, package_delay)
            elif 'GTR' in d[1]:
                print("skipping GTR for package delay", name)
            elif 'JTAG' in d[1]:
                print("skipping JTAG for package delay", name)
            elif 'PS_ERROR' in d[1]:
                print("skipping PS_ERROR for package delay", name)
            else:
                try:
                    ball = conn_to_ball[sname]
                    package_delay = package_lengths[ball[0]]
                except:
                    print("fail other", conn, name, som_delay, package_delay, l)
            #print(name, som_delay, package_delay)
            delay[name] = round(som_delay+package_delay, 1)
    with open(f"som_delays_{conn}.json", "w") as f:
        json.dump(delay, f, indent=4)

if __name__ == '__main__':
    get_lengths('JA1')
    get_lengths('JA2')
