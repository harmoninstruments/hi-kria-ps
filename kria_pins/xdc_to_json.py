# Copyright 2022 Harmon Instruments, LLC
# MIT license
# converts Xilinx provided Kria SOM XDC to JSON

import shlex
import json

def parse():
    som = [{}, {}]
    # file not provided due to copyright, only needed to regenerate JSON
    with open("Kria_K26_SOM_Rev1.xdc", "r") as f:
        for line in f:
            if len(line) == 0:
                continue
            if line[0] == '#':
                continue
            l = shlex.split(line)
            if len(l) == 0:
                continue
            if l[0:2] != ["set_property","PACKAGE_PIN"]:
                continue
            l = l[2:]
            if l[2] == 'No Connect]':
                continue

            ball = l[0]
            name = l[-1]
            connpin = l[2][:-1]
            if 'som240_' not in connpin:
                continue
            somconn = 0 if 'som240_1' in connpin else 1
            connpin = connpin[9:]
            som[somconn][connpin.upper()] = [ball, name]
    return som

def write(data, i):
    with open(f"som{i}.json", "w") as f:
        json.dump(data, f, indent=4)

if __name__ == '__main__':
    som = parse()
    write(som[0], 1)
    write(som[1], 2)
