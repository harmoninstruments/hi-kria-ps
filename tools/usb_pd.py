#!/usr/bin/env python3
# Copyright (c) 2014-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: MIT

from struct import pack, unpack

def print_pdo(pdo, i):
    pdotype = pdo >> 30

    if pdotype == 0:
        current = int(pdo & 0x3FF) * 0.01
        voltage = int((pdo >> 10) & 0x3FF) * 0.05
        peakcurrent = int((pdo >> 20) & 0x3)
        print(f'PDO {i} = 0x{pdo:08X} fixed, {voltage} V, {current} A, peak {peakcurrent}')
    elif pdotype == 3: # APDO, PPS
        current = int(pdo & 0x7F) * 0.05
        voltage_min = int((pdo >> 8) & 0x7F) * 0.1
        voltage_max = int((pdo >> 17) & 0xFF) * 0.1
        print(f'PDO {i} = 0x{pdo:08X} PPS, {voltage_min} V min, {voltage_max} V max, {current} A')
    else:
        print(f'PDO {i} = 0x{pdo:08X} other')

def print_pd_info(data):
    rv = unpack("HHHHHHIIIIIIIBBHHBB", data)
    print(rv)
    print(f'PHY VID:PID {rv[0]:04X}:{rv[1]:04X}')
    print(f'devid: {rv[2]:04X}')
    print(f'typecrev: {rv[3]:04X}')
    print(f'usbpdrev: {rv[4]:04X}')
    print(f'pdifrev: {rv[5]:04X}')
    npdos = rv[13]
    pdos = rv[6:13]
    print(f'number of PDOs: {npdos}')
    for i in range(min(npdos, len(pdos))):
        print_pdo(pdos[i], i)
    print(f'requested PDO: {rv[14]}')
    vbus_adc_raw = rv[15]
    vbus_scale = 0.025 # TCPC.hpp vbus_adc
    vbus = vbus_adc_raw * vbus_scale
    print(f'VBUS ADC: {vbus:.3} V')
    print(f'state: {rv[16]}')
    print(f'orientation: {rv[17]}')
    print(f'PD version: {rv[18]+1}')

def print_packets(data):
    rv = unpack("BBHIIIIIII", data)
    print(rv)

    plen = rv[0]
    if plen == 0:
        return
    desc = 'unknown'
    header = rv[2]
    msgtype = header & 0x1F
    if plen == 3:
        if msgtype == 3:
            desc = 'Accept'
        if msgtype == 6:
            desc = 'PS ready'
    else:
        if msgtype == 1:
            if (header & (1<<15)) != 0:
                desc = 'Source capabilities extended'
                vid = rv[3] & 0xFFFF
                pid = rv[3] >> 16
                xid = rv[4]
                fw_version = rv[5] & 0xFF
                hw_version = (rv[5] >> 8) & 0xFF
                voltage_regulation = (rv[5] >> 16) & 0xFF
                holdup_time = (rv[5] >> 24) & 0xFF
                compliance = rv[6] & 0xFF # bit 0 LPS, 1, PS1, 2 PS2
                # 0: low touch current eps, 1: ground pin supported when set, 2: ground pin intended for protective earth set
                touch_current = (rv[6] >> 8) & 0xFF
                peak_current_1 = (rv[6] >> 16) & 0xFFFF # 0-4: percent overload 10% inc, 5-10: overload period 20ms, 11-14: duty cycle in 5%, 15: vbus droop
                peak_current_2 = (rv[7] >> 0) & 0xFFFF
                peak_current_3 = (rv[7] >> 16) & 0xFFFF
                touch_temp = (rv[8] >> 0) & 0xFF # 0 IEC6095-1 1: IEC6238-1 TS1 2: IEC6238-1 TS2
                source_inputs = (rv[8] >> 8) & 0xFF # 0: external supply present 1: external supply is unconstrained 2: internal battery present
                num_batts = (rv[8] >> 16) & 0xFF
                source_pdp = (rv[8] >> 24) & 0x7F
                print(f"Source capabilities extended:\n\tVID: {vid:04X}\n\tPID: {pid:04X}")
                print(f"\txid = {xid:08X}\n\tfw_version = {fw_version:02X}")
                print(f"\thw_version = {hw_version:02X}")
                print(f"\tvoltage regulation = {voltage_regulation:02X}")
                print(f"\tholdup time = {holdup_time:02X}")
                print(f"\tcompliance = {compliance:02X}\n\ttouch_current = {touch_current:02X}")
                print(f"\tpeak current {peak_current_1:04X} {peak_current_2:04X} {peak_current_3:04X}")
                print(f"\ttouch temp {touch_temp:02X}\n\tsource inputs {source_inputs:02X}")
                print(f"\tnum_batts = {num_batts:02X}\n\tsource pdp = {source_pdp:02X}")
                return
            else:
                desc = 'Source capabilities'
        if msgtype == 15:
            vdm_hdr = rv[3]
            desc = 'Vendor defined'
            if (vdm_hdr & (1<<15)) != 0:
                desc += " structured"
            else:
                desc += " unstructured"
            command = vdm_hdr & 0x5
            if command == 2:
                desc += " Discover SVIDs"
    rev = 1 + (0x3&(rv[2] >> 6))
    print(f'PD packet {desc}: len={plen}, type={rv[1]:02X}, header={rv[2]:04X}, rev={rev}')

    for i in range((plen-3)//4):
        print(f'{rv[3+i]:08X}')
