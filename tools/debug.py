#!/usr/bin/env python3
#!/usr/bin/env python3
# Copyright (c) 2022 Harmon Instruments, LLC
# SPDX-License-Identifier: MIT

import argparse
import time
import usb.core
import usb.util
import sys
from struct import pack, unpack
from datetime import datetime
from jtag import HiKriaJTAG, XilinxVirtualCable
import usb_pd

class HiKriaDebug:
    def __init__(self, serial = None):
        self.dev = None
        devs = usb.core.find(find_all = 1, idVendor=0xDEAD, idProduct=0xBEEF)
        print("found devices:")
        for dev in devs:
            print(dev.product, dev.serial_number)
            if dev.product == 'hi-kria debug':
                if serial in dev.serial_number:
                    self.dev = dev
        if self.dev is None:
            raise ValueError('Device not found')
        #self.dev.set_interface_altsetting(interface = 2, alternate_setting = 0)
        #self.dev.set_configuration()
        self.cfg = self.dev.get_active_configuration()
        self.intf = self.cfg[(0,0)]
        dev = self.dev
        print("Manufacturer =", usb.util.get_string(dev, dev.iManufacturer))
        print("Product = ", usb.util.get_string(dev, dev.iProduct))
        print("Serial = ", usb.util.get_string(dev, dev.iSerialNumber))

    # magic numbers for the debug commands
    debug_magic = {
        'bootmode_emmc': 0x286593a8,
        'bootmode_qspi': 0x286593a9,
        'bootmode_jtag': 0x286593aa,
        'jtag_bits': 0x94440000,
        'jtag_memwrite': 0x94450000,
        'reset_to_bootloader': 0xf8534353,
        'power_off': 0xb12884fb,
        'pd_info': 0xd19d94f2,
        'pd_rxdata': 0xd19d94f3,
        'read_adc': 0xe4e1f00a,
    }

    def bulk_out(self, d):
        # send a bulk USB packet to the debug controller, up to 64 bytes
        return self.dev.write(4, d)

    def bulk_in(self, n):
        # receive a bulk USB packet from the debug controller
        # blocks for up to 0.5 seconds
        rv = bytes(self.dev.read(0x84, n, timeout=500))
        return rv

    def out_1(self, a):
        # send a single 32 bit word to the debug controller
        self.bulk_out(pack("I", a))

    def devinfo(self):
        self.out_1(0)
        rv = unpack("IIII", self.bulk_in(64)[:16])
        for i in range(4):
            print('info {} = 0x{:8X}'.format(i, rv[i]))
        return rv

    def pd_info(self):
        self.out_1(self.debug_magic['pd_info'])
        rv = self.bulk_in(48)
        usb_pd.print_pd_info(rv)

    def read_adc(self):
        self.out_1(self.debug_magic['read_adc'])
        (ivbus, i5v, vbus)= unpack("HHH", self.bulk_in(6))
        # the analog inputs have a 27 k pulldown
        # the DPS1133A switch outputs 10 uA/V
        sensor_gain = 1.0 / (27e3 * 1e-5) # A / V
        # the ADC voltage reference is 3.3 V, full scale is 4095
        code_to_v = 3.3 / 4095.0
        code_to_i = code_to_v * sensor_gain
        ivbus *= code_to_i
        vbus *= 0.025 # TCPC.hpp info.vbus_adc comment
        isom = i5v*code_to_i
        print(f'SOM 5 V:\n\t{isom:.3f} A {isom*5:.2f} W')
        print(f'Vbus:\n\t{vbus:.2f} V {ivbus:.2f} A {ivbus*vbus:.2f} W')
        return (vbus, ivbus, isom)

    def pd_rxdata(self):
        got_at_least_one_packet = False
        while True:
            self.out_1(self.debug_magic['pd_rxdata'])
            rv = self.bulk_in(32)
            if rv[0] == 255:
                if got_at_least_one_packet == False:
                    print("PD RX FIFO empty")
                break
            got_at_least_one_packet = True
            usb_pd.print_packets(rv)

    def reset_to_bootloader(self):
        self.out_1(self.debug_magic['reset_to_bootloader'])

    def set_boot(self, mode):
        """
        set the boot mode pins of the Zynq US+ SOC

        mode: "jtag", "qspi" or "emmc"

        """
        mode = mode.lower()
        print(f"setting boot mode to {mode}")
        self.out_1(self.debug_magic['bootmode_'+mode])

    def jtag(self, nbits, tms, tdi, tdo=True):
        """
        do a JTAG transfer to the SOC

        nbits: number of bits to transfer

        tms, tdi: bytes, sent LSB of byte 0 first

        tdo: bool, True to return the TDO data, False not not (faster)

        """
        assert len(tms) == len(tdi)
        assert nbits > 0
        bits_requested = 0
        while bits_requested < nbits:
            chunk_bits = min(240, nbits - bits_requested)
            command = self.debug_magic['jtag_bits'] + chunk_bits
            if tdo:
                command |= 0x8000
            start = bits_requested >> 3
            stop = start + ((chunk_bits + 7) >> 3)
            odata = pack("I", command) + tms[start:stop] + tdi[start:stop]
            dev.bulk_out(odata)
            bits_requested += chunk_bits
        if not tdo:
            return None
        idata = bytes(0)
        nbytes = (nbits+7)>>3
        while len(idata) < nbytes:
            remain = nbytes-len(idata)
            idata += dev.bulk_in(min(remain, 30))
        return idata

    def jtag_memwrite(self, data):
        """
        do a memory write via JTAG and the ARM debug port
        this is only the data phase, address is set up via
        jtag()

        data: bytes

        """
        # pad to multiple of 4 bytes
        if len(data) & 3 != 0:
            data += bytes(4-(len(data) & 3))
        while len(data) > 0:
            words = min(15, len(data) // 4)
            command = self.debug_magic['jtag_memwrite'] | words | 0x0400
            odata = pack("I", command) + data[:4*words]
            dev.bulk_out(odata)
            data = data[4*words:]

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--xvc', action='store_true', help="run Xilinx Virtual Cable server")
    parser.add_argument('--jtagboot', action='store_true', help="boot via JTAG")
    parser.add_argument('--reset_to_bootloader', action='store_true', help="enter bootloader")
    parser.add_argument('--pd_info', action='store_true', help="display USB PD info")
    parser.add_argument('--read_adc', action='store_true', help="read ADC")
    parser.add_argument('--pd_rxdata', action='store_true', help="display USB PD RX data")
    parser.add_argument('--boot', type=str, help="set the boot mode [emmc, qspi, jtag]", default=None)
    parser.add_argument('--svf', type=str, help="SVF file to play", default=None)
    parser.add_argument('--serial', type=str, help="board serial number", default = '')
    args = parser.parse_args()

    if(args.reset_to_bootloader):
        try:
            dev = HiKriaDebug(serial=args.serial)
            dev.reset_to_bootloader()
        except:
            print("no device found, may already be in bootloader")
        exit()

    dev = HiKriaDebug(serial=args.serial)
    dev.devinfo()
    if(args.boot is not None):
        dev.set_boot(args.boot)

    jtag = HiKriaJTAG(dev)

    if(args.xvc):
        XilinxVirtualCable(dev)
    if(args.svf is not None):
        jtag.svf_file(args.svf)

    if(args.jtagboot):
        jtag.dapcon()
        jtag.memwrite(0xffca0038, 0x1ff) # disable gates in CSU JTAG security register
        time.sleep(0.1)
        jtag.pmu_stop()
        jtag.load_file("../boot/pmufw/pmufw.bin", addr=0xffdc0000)
        jtag.pmu_start()
        time.sleep(0.2)
        jtag.enable_debug()
        jtag.memwrite(0xffff0000, 0x14000000) # bootloop
        jtag.memwrite(0xfd1a0104, 0x0000380e) # release A53 reset
        jtag.stop_a53()
        time.sleep(0.1)
        jtag.load_file("../boot/fsbl/fsbl.bin", addr=0xfffc0000)
        jtag.set_pc(0xfffc0000)
        time.sleep(0.1)
        jtag.continue_a53()
        time.sleep(1) # time for FSBL to run
        jtag.stop_a53()
        jtag.load_file("../boot/atf/bl31.bin",        addr = 0xfffea000)
        jtag.load_file("../boot/u-boot/u-boot.bin",   addr = 0x08000000)
        jtag.set_pc(0xfffea000)
        jtag.continue_a53()

    if(args.pd_info):
        dev.pd_info()

    if(args.pd_rxdata):
        dev.pd_rxdata()

    if(args.read_adc):
        dev.read_adc()
