#!/usr/bin/env python3
import urllib.request, hashlib, argparse, os, sys

parser = argparse.ArgumentParser()
parser.add_argument('--sha', type=str, help="expected SHA-256", default=None)
parser.add_argument('--url', type=str, help="file download URL")
parser.add_argument('--file', type=str, help="file storage location")
args = parser.parse_args()

# see if we already have the file
try:
    with open(args.file, "rb") as f:
        data = f.read()
        sha = hashlib.sha256(data).hexdigest()
        if args.sha is None:
            print(f"found file {args.file} with SHA {sha}")
            sys.exit(os.EX_OK)
        if sha == args.sha:
            print(f"found file {args.file} with correct SHA {sha}")
            sys.exit(os.EX_OK)
        print(f"found file {args.file} with incorrect SHA {sha}, removing")
        os.remove(args.file)
except FileNotFoundError:
    print(f"failed to open file {args.file}")

print(f"file {args.file} not found, attempting download")

url = args.url
with urllib.request.urlopen(url) as response:
   data = response.read()
   sha = hashlib.sha256(data).hexdigest()
   if sha == args.sha or args.sha is None:
       print(f"success, SHA {sha}")
       with open(args.file, "wb") as f:
           f.write(data)

sys.exit(os.EX_UNAVAILABLE)
