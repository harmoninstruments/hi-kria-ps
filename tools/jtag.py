#!/usr/bin/env python3
import time
import usb.core
import usb.util
import socket
import sys
from struct import pack, unpack

def progressbar(it, prefix="", size=60, file=sys.stdout):
    count = len(it)
    def show(j):
        x = int(size*j/count)
        b = '#'*x+'.'*(size-x)
        pct = 100*j/count
        file.write(f"{prefix}[{b}] {pct:3.1f}%\r")
        file.flush()
    show(0)
    for i, item in enumerate(it):
        yield item
        show(i+1)
    file.write("\n")
    file.flush()

class HiKriaJTAG:
    # just enough for JTAG boot, mostly reverse engineered from XSCT SVFs
    def __init__(self, dev):
        self.dev = dev
        self.jtag = dev.jtag

    def jtag_bytes(self, tms, tdi, tdo=None, mask=None, bits=None):
        rv = self.jtag(8*len(tms) if bits is None else bits, tms, tdi, tdo=tdo is not None)
        if tdo is None:
            return
        assert len(tms) == len(tdo)
        assert len(tdo) == len(mask)
        for i in range(len(tms)):
            assert rv[i] & mask[i] == tdo[i] & mask[i]

    def dp_sir4(self, val):
        self.jtag_bytes(tms=pack("I", int('1100000000000000000110'[::-1], 2)),
                        tdi=pack("I", (val << 4) | (0xFFF << 8)))
    def sel_dpacc(self):
        self.dp_sir4(0xA) # SIR 4 TDI (a) SMASK (f);
    def sel_apacc(self):
        self.dp_sir4(0xB) # SIR 4 TDI (b) SMASK (f);
    def jtag_dap_sdr35(self, val):
        self.jtag_bytes(
            tms=pack("Q", int('10000000000000000000000000000000000000110'[::-1], 2))[:6],
            tdi=pack("Q", val<<3)[:6],
        )
    def dp_w(self, a, d):
        self.jtag_bytes(
            tms=pack("Q", int('10000000000000000000000000000000000000110'[::-1], 2))[:6],
            tdi=pack("Q", (a<<3) | (d<<6))[:6],
        )
    def dp_w27(self, a, d):
        # same as dp_w but expect TDO=2 with a mask of 7
        self.jtag_bytes(
            tms=pack("Q", int('10000000000000000000000000000000000000110'[::-1], 2))[:6],
            tdi=pack("Q", (a<<3) | (d<<6))[:6],
            tdo=pack("Q", 2<<3)[:6],
            mask=pack("Q", 7<<3)[:6],
        )

    def reset(self):
        self.jtag_bytes(tms=pack("B", int("1111110"[::-1], 2)), tdi=bytes(1))
    def runtest(self, n):
        while n > 0:
            self.jtag(240, bytes(30), bytes(30))
            n -= 240
    def dapcon(self):
        # enable debug access port
        # SIR 12 TDI (824), SDR 32 TDI (3)
        self.reset()
        self.jtag_bytes(
            tms=pack("Q", int("110000000000000000011010000000000000000000000000000000000110"[::-1],2)),
            tdi=pack("Q", int("000011110010010000010000001100000000000000000000000000000000"[::-1],2)),
        )
        self.runtest(1000)
        self.reset()
        self.runtest(1000)

    # do a 32 bit memory write
    def memwrite(self, addr, value):
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x00000000)
        self.sel_apacc()
        self.dp_w27(0, 0x30006002)
        self.dp_w27(2, addr)
        self.dp_w27(6, value)
        self.sel_dpacc()
        self.dp_w(3, 0x00000000) # MASK(0);
        self.dp_w(7, 0x00000000) # TDO (0) MASK(20);

    def continue_a53(self):
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x01000000)
        self.sel_apacc()
        self.dp_w27(0, 0x80000002)
        self.dp_w27(2, 0x80410300)
        self.dp_w27(6, 0x00000000)
        self.dp_w27(2, 0x80410088)
        self.dp_w27(6, 0x03006327)
        self.dp_w27(2, 0x80410084)
        self.runtest(100)
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x01000000)
        self.sel_apacc()
        self.dp_w27(0, 0x80000002)
        self.dp_w27(2, 0x80420fb0)
        self.dp_w27(6, 0xc5acce55)
        self.dp_w27(2, 0x80410090)
        self.dp_w27(6, 0x00000008)
        self.dp_w27(2, 0x80420140)
        self.dp_w27(6, 0x00000000)
        self.dp_w27(2, 0x80420000)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x804200a0)
        self.dp_w27(6, 0x00000000)
        self.dp_w27(2, 0x80420018)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x80420010)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x804200a4)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x8042001c)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x80410088)
        self.dp_w27(7, 0x00000000)
        self.sel_dpacc()
        self.dp_w(3, 0x00000000) # TDO (2) MASK(e800000a);
        self.dp_w(7, 0x00000000) # TDO (0) MASK(20);
        self.runtest(1000)

    def stop_a53(self):
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x01000000)
        self.sel_apacc()
        self.dp_w27(0, 0x80000002)
        self.dp_w27(2, 0x80410300)
        self.dp_w27(6, 0x00000000)
        self.dp_w27(2, 0x80410088)
        self.dp_w27(6, 0x02007c02)
        self.dp_w27(2, 0x80420fb0)
        self.dp_w27(6, 0xc5acce55)
        self.dp_w27(2, 0x80420140)
        self.dp_w27(6, 0x00000000)
        self.dp_w27(2, 0x80420000)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x804200a4)
        self.dp_w27(6, 0x00000000)
        self.dp_w27(2, 0x80420010)
        self.dp_w27(6, 0x00000002)
        self.dp_w27(2, 0x80420018)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x804200a0)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x80420014)
        self.dp_w27(6, 0x00000001)
        self.dp_w27(2, 0x80420018)
        self.dp_w27(6, 0x00000001)
        self.sel_dpacc()
        self.dp_w(3, 0x00000000) # TDO (2) MASK(0);
        self.dp_w(7, 0x00000000) # TDO (0) MASK(20);
        self.runtest(100)
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x01000000)
        self.sel_apacc()
        self.dp_w27(0, 0x80000002)
        self.dp_w27(2, 0x80410088)
        self.dp_w27(7, 0x00000000)
        self.sel_dpacc()
        self.dp_w(3, 0x00000000) # TDO (800000a) MASK(e800000a);
        self.dp_w(7, 0x00000000) # TDO (0) MASK(20);
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x01000000)
        self.sel_apacc()
        self.dp_w27(0, 0x80000002)
        self.dp_w27(2, 0x80410084)
        self.runtest(1000)

    def svf_file(self, fn):
        print("playing SVF:", fn)
        with open(fn, "rb") as f:
            self.svf(f.read())
    # svf is a string containing SVF data
    def svf(self, svf, verbose=False, trim_tail=0):
        command = ['libxsvf/svfconvert']
        if verbose:
            command.append("-v")
        rv = subprocess.run(command, input=svf, stdout=subprocess.PIPE)
        #rv = subprocess.run(['svf/convert'], input=svf, stdout=subprocess.PIPE)
        bits, = unpack("Q", rv.stdout[:8])
        nb = (bits+7)//8
        assert len(rv.stdout) == (8 + 4 * nb)
        rv = rv.stdout[8:]
        tms = rv[:nb]
        tdi = rv[nb:2*nb]
        tdo = rv[2*nb:3*nb]
        mask = rv[3*nb:4*nb]
        print(f"starting SVF playback of {bits} bits, {nb} bytes")
        bits -= trim_tail
        for i in progressbar(range((bits+239)//240), "svf: ", 40):
            bits_processed = i*240
            start = i*30
            bits_packet = min(240, bits-(i*240))
            bytes_packet = (bits_packet + 7) >> 3
            check_tdo = True
            if mask[i*30:i*30+bytes_packet] == bytes(bytes_packet):
                check_tdo = False
            tdo_packet = self.jtag(
                bits_packet,
                tms[i*30:i*30+bytes_packet],
                tdi[i*30:i*30+bytes_packet],
                tdo=check_tdo)
            fail = False
            if check_tdo:
                for j in range(bytes_packet):
                    if tdo_packet[j] & mask[i*30+j] != tdo[i*30+j] & mask[i*30+j]:
                        fail = True
            def getbit(x, n):
                b = x[n>>3]
                return (b & (1<<(n&7))) != 0
            if fail:
                print("tdo fail at bit", bits_processed)
                print("tms:", end='')
                for i in range(bits_packet):
                    print('1' if getbit(tms, i) else '0', end='', sep='')
                print('')
                print("tdi:", end='')
                for i in range(bits_packet):
                    print('1' if getbit(tdi, i) else '0', end='', sep='')
                print('')
                print("oex:", end='')
                for i in range(bits_packet):
                    c = '1' if getbit(tdo, i) else '0'
                    print(c if getbit(mask, i) else '-', end='', sep='')
                print('')
                print("oac:", end='')
                for i in range(bits_packet):
                    print('1' if getbit(tdo_packet, i) else '0', end='', sep='')
                print('')

    def enable_debug(self):
        self.reset()
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0) # select - APSEL=FF, DPBANKSEL=0, APBANKSEL=F
        self.dp_w27(2, 0x50000003) # ctrl/stat
        self.dp_w27(2, 0x50000001) # ctrl/stat
        self.dp_w(3, 0) # read ctrl/stat TDO (2) MASK(0);
        self.dp_w(7, 0) # read buff SDR 35 TDI (7) TDO (0) MASK(20);
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0)
        self.sel_apacc()
        self.dp_w27(0, 0x30006002)
        self.sel_dpacc()
        self.dp_w(3, 0) # TDO (2) MASK(0);
        self.dp_w(7, 0) # TDO (0) MASK(20);
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x1000000)
        self.sel_apacc()
        self.dp_w27(0, 0x80000002)
        self.sel_dpacc()
        self.dp_w(3, 0) #TDO (2) MASK(0);
        self.dp_w(7, 0) #TDO (0) MASK(20);

    def load_file(self, fn, addr):
        assert (addr & 1023) == 0
        with open(fn, "rb") as f:
            data = f.read()
        if (len(data) & 0x3) != 0:
            data += bytes(4 - (len(data)&3))
        self.enable_debug()
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0)
        self.sel_apacc()
        self.dp_w27(0, 0x30006012)
        # have to send in 1024 byte chunks due to TAR limitation
        for i in progressbar(range((len(data)+1023)>>10), fn + ": ", 40):
            # set address
            self.dp_w27(2, addr + 1024*i)
            self.dev.jtag_memwrite(data[:1024])
            data = data[1024:]
        self.sel_dpacc()
        self.dp_w(0x3, 0) # TDO (2) MASK(0);
        self.dp_w(0x7, 0) # TDO (0) MASK(20);
        self.runtest(500)

    def set_pc(self, addr=0xfffc0000):
        self.sel_dpacc()
        self.dp_w27(4, 0xff0000f0)
        self.dp_w27(2, 0x50000003)
        self.dp_w27(4, 0x1000000)
        self.sel_apacc()
        self.dp_w(0, 0x80000002)
        self.dp_w(2, 0x80410300)
        self.dp_w(6, 0)
        self.dp_w(2, 0x80410088)
        self.dp_w(6, 0x3006327)
        self.dp_w(2, 0x8041008c)
        self.dp_w(6, 0)
        self.dp_w(2, 0x80410080)
        self.dp_w(6, addr)
        self.dp_w(2, 0x80410084)
        self.dp_w(6, 0xd5330400)
        self.dp_w(2, 0x80410084)
        self.dp_w(6, 0xd51b4520)
        self.sel_dpacc()
        self.jtag_dap_sdr35(3) # TDO (2) MASK(0);
        self.jtag_dap_sdr35(7) # TDO (0) MASK(20);

    def pmu_sir(self, val):
        self.jtag_bytes(tms=pack("I", int('1100000000000000000110'[::-1], 2)),
                        tdi=pack("I", (0xff << 4) | (val << 8)),
                        bits=22)
    def pmu_sdr(self, n, val):
        self.jtag_bytes(tms=pack("I", 1 | (3<<(n+3))),
                        tdi=pack("I", (val << 4)),
                        bits=n+6)
    def pmu_select(self):
        self.pmu_sir(0xfff)
        self.pmu_sir(0x0e4)
        # select MDM
        self.pmu_sdr(4,1)
        # select mb
        self.pmu_sdr(8, 0xd)
        self.pmu_sdr(8, 1)

    def pmu_stop(self):
        self.pmu_select()
        # stop
        self.pmu_sdr(8, 1)
        self.pmu_sdr(10, 0x0249)
        self.pmu_sdr(8, 1)
        self.pmu_sdr(10, 0x0040)
        self.pmu_sdr(8, 1)
        self.pmu_sdr(10, 0x0048)
        self.runtest(1000)

    def pmu_start(self):
        self.pmu_select()
        # set pc to 0xffdd0cec
        self.pmu_sdr(8,4)
        def sdr72_magic(): # do SDR 72 TDI (3730101dbbff000d69);
            self.jtag_bytes(
                tms=pack("H", 1) + pack("Q", 3<<59),
                tdi=pack("H", 0xD690) + pack("Q", 0x3730101dbbff000),
                bits=78)
        sdr72_magic()
        self.pmu_sdr(8,1)
        self.pmu_sdr(10, 0x0105)
        self.pmu_sdr(8,4)
        sdr72_magic()
        # continue
        self.pmu_sdr(8,1)
        self.pmu_sdr(10, 0x0105)
        self.runtest(1000)


def XilinxVirtualCable(dev):
    # Set up a TCP/IP server
    tcp_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    tcp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    tcp_socket.setsockopt(socket.IPPROTO_TCP, socket.TCP_NODELAY, 1)

    # Bind the socket to server address and port 81
    server_address = ('localhost', 2542)
    tcp_socket.bind(server_address)

    # Listen on port 81
    tcp_socket.listen(1)

    while True:
        print("Waiting for connection")
        connection, client = tcp_socket.accept()
        try:
            print("Connected to client IP: {}".format(client))
            data = bytes(0)
            while True:
                if(len(data) < 8):
                    data += connection.recv(163840)
                #print("Received data: {}".format(data))
                if not data:
                    break
                if data == b'getinfo:':
                    connection.send(b"xvcServer_v1.0:8192\n")
                    data = bytes(0)
                    continue
                if b'settck:' in data:
                    connection.send(pack("I", 100))
                    data = bytes(0)
                    continue
                if b'shift:' in data:
                    if(len(data) < 10):
                        data += connection.recv(163840)
                    nbits, = unpack("I", data[6:10])
                    data = data[10:] # clip header

                    nbytes = (nbits+7)//8
                    if(len(data) < (2*nbytes)):
                        data += connection.recv(163840)
                    tdo = bytes(0)
                    tms = data[:nbytes]
                    tdi = data[nbytes:2*nbytes]
                    data = data[2*nbytes:]
                    print("TMS", "TDI", len(tms), len(tdi), nbytes, nbits)

                    while True:
                        bits_packet = min(8*30, nbits)
                        bytes_packet = (bits_packet + 7) // 8
                        #print("bits = ", bits_packet, bytes_packet, len(tms))
                        tdo += dev.jtag(bits_packet, tms[:bytes_packet], tdi[:bytes_packet])
                        if(len(tdi) == bytes_packet):
                            break
                        nbits -= 8*bytes_packet
                        tms = tms[bytes_packet:]
                        tdi = tdi[bytes_packet:]
                    connection.send(tdo)
        finally:
            connection.close()
