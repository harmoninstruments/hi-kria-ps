#!/bin/bash
source ../../env.sh
mkdir -p $DL_DIR
VER=v2022.1
FILE=$DL_DIR/bootgen_$VER.tar.gz
download.py --url https://github.com/Xilinx/bootgen/archive/refs/tags/xilinx_$VER.tar.gz \
            --file $FILE \
            --sha a7db095abda9820babbd0406e7036d663e89e8c7c27696bf4227d8a2a4276d13
rm -rf build
mkdir build
tar xf $FILE -C build --strip-components 1
cd build
make -j$(nproc)
cp bootgen ..
