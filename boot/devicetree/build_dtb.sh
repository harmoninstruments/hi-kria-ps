#!/bin/bash
gcc -E -nostdinc -undef -D__DTS__ -x assembler-with-cpp -o hi-kria.dts.preprocessed hi-kria.dts -I ../linux/build/include/ -I ../linux/build/arch/arm64/boot/dts/xilinx/
dtc hi-kria.dts.preprocessed > hi-kria.dtb
dtc hi-kria.dtb > hi-kria.processed.dts
