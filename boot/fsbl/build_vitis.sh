#!/bin/bash
source $XILINX/Vitis/2022.1/settings64.sh
source ../../env.sh
export SOURCE_DATE_EPOCH=$(git log -1 --pretty=%ct)
rm -rf build_vitis
mkdir -p build_vitis
xsct build_fsbl.tcl
setup_aarch64_none
make -C build_vitis/ all
