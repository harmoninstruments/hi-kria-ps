#!/bin/bash
set -x
source ../../env.sh
setup_aarch64_none

FILE=$DL_DIR/embeddedsw_v2022.1.tar.gz
download.py --url https://github.com/Xilinx/embeddedsw/archive/refs/tags/xilinx_v2022.1.tar.gz \
            --file $FILE \
            --sha 5ed25477197eab2303bfa13e7af15e32d5b62979327ea250f98859d478cff11f
rm -rf build
mkdir -p build
tar xf $FILE -C build --strip-components 1
FSBL_PATH=build/lib/sw_apps/zynqmp_fsbl/src
cp -ra hi-kria $FSBL_PATH/../misc/
cp $REPO_TOP/fpga/build_cores/hi-kria-cores.gen/sources_1/bd/kria_bd/ip/kria_bd_zynq_ultra_ps_e_0_0/psu_init.* $FSBL_PATH/../misc/hi-kria/
cp build/lib/bsp/standalone/src/arm/ARMv8/64bit/platform/ZynqMP/xparameters_ps.h $FSBL_PATH/../misc/hi-kria/a53/
# don't pass -j as it does a parallel build itself
make BOARD=hi-kria DUMP=true CFLAGS+=-DFSBL_DEBUG_INFO CFLAGS+=-DXFSBL_PERF -C $FSBL_PATH
cp $FSBL_PATH/fsbl.elf fsbl.elf
${CROSS_COMPILE}objcopy -O binary fsbl.elf fsbl.bin
