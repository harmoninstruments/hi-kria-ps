#!/bin/bash
source ../../env.sh
export PATH=$PATH:$XILINX/Vitis/2022.1/gnu/microblaze/lin/bin
export CROSS_COMPILE=mb-
export ARCH=microblaze
export SOURCE_DATE_EPOCH=$(git log -1 --pretty=%ct)
FILE=$DL_DIR/embeddedsw_v2022.1.tar.gz
download.py --url https://github.com/Xilinx/embeddedsw/archive/refs/tags/xilinx_v2022.1.tar.gz \
            --file $FILE \
            --sha 5ed25477197eab2303bfa13e7af15e32d5b62979327ea250f98859d478cff11f
rm -rf build
mkdir -p build
tar xf $FILE -C build --strip-components 1
PMUPATH=build/lib/sw_apps/zynqmp_pmufw/src
make -C ${PMUPATH}
cp ${PMUPATH}/executable.elf pmufw.elf
mb-objcopy -O binary pmufw.elf pmufw.bin
