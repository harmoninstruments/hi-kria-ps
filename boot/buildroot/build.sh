#!/bin/bash
source ../download.sh
FILE=buildroot-2022.02.tar.xz
mkdir -p ../dl
download https://buildroot.org/downloads/${FILE} ../dl/${FILE} 8161c43dc6a11c0bc86588c09a8e1dc935b28ca046447ad02bf7074064456701
rm -rf build
mkdir build
tar xf ../dl/${FILE} -C build --strip-components 1
cd build
cp ../hi-kria_defconfig configs
make hi-kria_defconfig
make
