verify_checksum() {
    cat $1 | sha256sum -c <(echo "$2  -") || rm -f $1
}

download() {
    # if it already exists, this will delete it if the checksum is wrong
    if [ -f $2 ]; then
        echo "existing file found" $2 "verifying sha"
        verify_checksum $2 $3
    fi
    if [ ! -f "$2" ]; then
        curl -s -L $1 | tee $2 | sha256sum -c <(echo "$3  -") || rm -f $2
    fi
}
