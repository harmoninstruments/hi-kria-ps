#!/bin/bash
source ../../env.sh
setup_aarch64_none
FILE=$DL_DIR/u-boot-xilinx-v2021.2.tar.gz
mkdir -p $DL_DIR
download.py \
    --url https://github.com/Xilinx/u-boot-xlnx/archive/refs/tags/xilinx-v2021.2.tar.gz \
    --file ${FILE} \
    --sha 4951b9a4601d8a5039171ffd0afe326cc51ff2beb5d80c89d1fba8283d9f48b1
rm -rf build
mkdir build
tar xf ${FILE} -C build --strip-components 1
cd build
cp ../defconfig configs/xilinx_zynqmp_virt_defconfig
cp ../../devicetree/hi-kria.{dts,dtb} arch/arm/dts/
cp ../zynqmp.c board/xilinx/zynqmp/zynqmp.c
cp ../env.txt .
export SOURCE_DATE_EPOCH=$(git log -1 --pretty=%ct)
make xilinx_zynqmp_virt_defconfig
make DEVICE_TREE=hi-kria -j$(nproc) u-boot.elf
${CROSS_COMPILE}size u-boot.elf
cp u-boot.elf ..
cd ..
${CROSS_COMPILE}objcopy -O binary u-boot.elf u-boot.bin
sha256sum u-boot.elf u-boot.bin
