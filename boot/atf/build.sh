#!/bin/bash
source ../../env.sh
echo "sourcing toolchain"
setup_aarch64_none
echo "fetching code"
FILE=$DL_DIR/atf.tar.gz
mkdir -p $DL_DIR
download.py \
    --url https://github.com/Xilinx/arm-trusted-firmware/archive/refs/tags/xilinx-v2022.1.tar.gz \
    --file $FILE \
    --sha e7d6a4f30d35b19ec54d27e126e7edc2c6a9ad6d53940c6b04aa1b782c55284e
rm -rf build
mkdir build
echo "extracting archive"
tar xf ${FILE} -C build --strip-components 1
cd build
echo "building"
make -j LOG_LEVEL=30 RESET_TO_BL31=1 V=1 PLAT=zynqmp ZYNQMP_CONSOLE=cadence0
cp build/zynqmp/release/bl31/bl31.elf ../
cd ..
${CROSS_COMPILE}size bl31.elf
${CROSS_COMPILE}objcopy -O binary bl31.elf bl31.bin
sha256sum bl31.elf bl31.bin
