#!/bin/bash
source ../../env.sh
setup_aarch64
FILE=$DL_DIR/linux-xilinx-v2022.1.tar.gz
mkdir -p ../dl
download.py --url https://github.com/Xilinx/linux-xlnx/archive/refs/tags/xilinx-v2022.1.tar.gz \
            --file ${FILE} \
            #--sha af0dd4aa240292024b091fd78582dcf93aeeb32c9854184a1b4ec3b7cc284f62
rm -rf build
mkdir build
tar xf ${FILE} -C build --strip-components 1
cd build
cp ../defconfig arch/arm64/configs/hi-kria_defconfig
make hi-kria_defconfig
make Image -j$(nproc)
cp arch/arm64/boot/Image ..
sha256sum ../Image
