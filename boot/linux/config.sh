#!/bin/bash
# run after building Linux to edit the config
cd build
make ARCH=arm64 nconfig
make ARCH=arm64 savedefconfig
cp defconfig ..
