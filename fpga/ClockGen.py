from amaranth import *
from amaranth.lib.cdc import FFSynchronizer

class ClockGen(Elaboratable):
    def __init__(self):
        self.reset = Signal()
        self.psen = Signal()
        self.psincdec = Signal()
        self.psdone = Signal()
        self.pll_clk2000 = Signal()
        self.phyclk625 = Signal()
    def elaborate(self, platform):
        m = Module()
        clk100 = platform.request("clk100", 0) # 100 MHz input clock
        pll_fb = Signal()
        pll_clk500 = Signal()
        pll_locked = Signal()

        m.submodules.pll_1000 = Instance(
            "PLLE4_BASE",
            a_LOC="PLL_X0Y6",
            p_CLKFBOUT_MULT  = 10,
            p_CLKFBOUT_PHASE = 0.0,
            p_CLKIN_PERIOD = 10.0,
            p_CLKOUT0_DIVIDE = 2,
            p_CLKOUT0_DUTY_CYCLE = 0.5,
            p_CLKOUT0_PHASE = 0.0,
            p_CLKOUT1_DIVIDE = 2,
            p_CLKOUT1_DUTY_CYCLE = 0.5,
            p_CLKOUT1_PHASE = 0.0,
            p_CLKOUTPHY_MODE = "VCO_2X",
            p_DIVCLK_DIVIDE = 1,
            p_REF_JITTER = 0.001,
            p_STARTUP_WAIT = "FALSE",
            o_CLKFBOUT = pll_fb,
            o_CLKOUT0 = pll_clk500,
            o_CLKOUT0B = Signal(name="clkout0b"),
            o_CLKOUT1 = Signal(),
            o_CLKOUT1B = Signal(),
            o_CLKOUTPHY = self.pll_clk2000,
            o_LOCKED = pll_locked,
            i_CLKFBIN = pll_fb,
            i_CLKIN = clk100,
            i_CLKOUTPHYEN = C(0),
            i_PWRDWN = C(0),
            i_RST = C(0),
        )

        ready = Signal()
        m.submodules.startup = Instance(
            "STARTUPE3",
            o_EOS=ready,
            o_CFGCLK=Signal(),
            o_CFGMCLK=Signal(),
            o_DI=Signal(4),
            i_DO=C(0,4),
            i_DTS=C(0xF, 4),
            i_FCSBO=C(0,1),
            i_FCSBTS=C(1,1),
            i_GSR=C(0),
            i_GTS=C(0),
            i_KEYCLEARB=C(0),
            i_PACK=C(0),
            o_PREQ=Signal(),
            i_USRCCLKO=C(0),
            i_USRCCLKTS=C(0),
            i_USRDONEO=C(0),
            i_USRDONETS=C(0),
        )

        # 500 MHz domain
        m.domains.c500s = ClockDomain("c500s", reset_less=True) # 500 MHz startup clock
        clr500 = Signal(reset=1)
        m.submodules.sync_clr500 = FFSynchronizer(
            i=(~pll_locked) | (~ready),
            o=clr500,
            reset=1,
            o_domain='c500s')
        m.submodules.bufg_500s = Instance(
            "BUFG",
            i_I=pll_clk500,
            o_O=ClockSignal("c500s")
        )
        m.submodules.bufg_500 = Instance(
            "BUFGCE_DIV", p_BUFGCE_DIVIDE="1", i_I=pll_clk500,
            o_O=ClockSignal("c500"), i_CE=C(1), i_CLR=clr500)
        m.submodules.bufg_250 = Instance(
            "BUFGCE_DIV", p_BUFGCE_DIVIDE="2", i_I=pll_clk500,
            o_O=ClockSignal("c250"), i_CE=C(1), i_CLR=clr500)
        m.submodules.bufg_125 = Instance(
            "BUFGCE_DIV", p_BUFGCE_DIVIDE="4", i_I=pll_clk500,
            o_O=ClockSignal("sync"), i_CE=C(1), i_CLR=clr500)

        pll625_fb = Signal()
        pll625_locked = Signal()
        pll_clk625 = Signal()

        m.submodules.pll_1250 = Instance(
            "PLLE4_BASE",
            a_LOC="PLL_X0Y7",
            p_CLKFBOUT_MULT = 10,
            p_CLKFBOUT_PHASE = 0.0,
            p_CLKIN_PERIOD = 8.0,
            p_CLKOUT0_DIVIDE = 2,
            p_CLKOUT0_DUTY_CYCLE = 0.5,
            p_CLKOUT0_PHASE = 0.0,
            p_CLKOUT1_DIVIDE = 2,
            p_CLKOUT1_DUTY_CYCLE = 0.5,
            p_CLKOUT1_PHASE = 0.0,
            p_CLKOUTPHY_MODE = "VCO_HALF",
            p_DIVCLK_DIVIDE = 1,
            p_REF_JITTER = 0.005,
            p_STARTUP_WAIT = "FALSE",
            o_CLKFBOUT = pll625_fb,
            o_CLKOUT0 = pll_clk625,
            o_CLKOUT0B = Signal(),
            o_CLKOUT1 = Signal(),
            o_CLKOUT1B = Signal(),
            o_CLKOUTPHY = self.phyclk625,
            o_LOCKED = pll625_locked,
            i_CLKFBIN = pll625_fb,
            i_CLKIN = ClockSignal("sync"),
            i_CLKOUTPHYEN = C(0),
            i_PWRDWN = C(0),
            i_RST = self.reset | ~(pll_locked),
        )

        ce625 = Signal()
        m.domains.c625s = ClockDomain("c625s", reset_less=True)
        m.submodules.sync_ce625 = FFSynchronizer(
            i=pll625_locked,
            o=ce625,
            o_domain='c625s')

        m.submodules.bufg_625s = Instance(
            "BUFG",
            i_I=pll_clk625,
            o_O=ClockSignal("c625s")
        )

        m.submodules.bufg_625 = Instance(
            "BUFGCE",
            i_I=ClockSignal("c625s"),
            o_O=ClockSignal("c625"),
            i_CE=ce625,
        )

        reset_counter = Signal(8, reset=255)
        self.reset = Signal(reset=1)
        with m.If(reset_counter != 0):
            m.d.sync += reset_counter.eq(reset_counter - 1)
        with m.Else():
            m.d.sync +=  self.reset.eq(0)

        return m
"""
        m.submodules.mmcm_1250 = Instance(
            "MMCME4_ADV",
            p_STARTUP_WAIT="TRUE",
            p_CLKOUT6_USE_FINE_PS="TRUE",
            o_LOCKED=self.pll_locked,
            # phase shift
            i_PSCLK=ClockSignal(),
            i_PSEN=self.psen,
            i_PSINCDEC=self.psincdec,
            o_PSDONE=self.psdone,
            i_PWRDWN=C(0),
            i_RST=C(0),
            # VCO @ 1.25GHz
            p_REF_JITTER1=0.01, p_CLKIN1_PERIOD=10.0,
            p_CLKFBOUT_MULT_F=15, p_DIVCLK_DIVIDE=1,
            i_CLKIN1=clk100,
            i_CLKIN2=C(0),
            i_CLKINSEL=C(1), # 1 is CLKIN1 for some reason
            i_CLKFBIN=pll_fb,
            o_CLKINSTOPPED=Signal(name="clkinstopped"),
            o_CLKFBSTOPPED=Signal(name="clkfbstopped"),
            o_CLKFBOUT=pll_fb,
            o_CLKFBOUTB=Signal(name='clkfboutb'),
            # DRP
            i_DADDR=Repl(0,7),
            i_DCLK=C(0),
            i_DEN=C(0),
            i_DWE=C(0),
            i_DI=Repl(0,16),
            o_DO=Signal(16, name='drp_do'),
            o_DRDY=Signal(),
            # outputs
            p_CLKOUT0_DIVIDE_F=3, p_CLKOUT0_PHASE=0.0, o_CLKOUT0=pll_clk625, o_CLKOUT0B=Signal(),
            p_CLKOUT1_DIVIDE=2, p_CLKOUT1_PHASE=0.0, o_CLKOUT1=Signal(name='clkout1'), o_CLKOUT2B=Signal(),
            p_CLKOUT2_DIVIDE=8, p_CLKOUT2_PHASE=0.0, o_CLKOUT2=Signal(name='clkout2'), o_CLKOUT2B=Signal(),
            p_CLKOUT3_DIVIDE=5, p_CLKOUT3_PHASE=0.0, o_CLKOUT3=Signal(name='clkout3'), o_CLKOUT3B=Signal(),
            p_CLKOUT4_DIVIDE=8, p_CLKOUT4_PHASE=0.0, o_CLKOUT4=Signal(name='clkout4'),
            p_CLKOUT5_DIVIDE=8, p_CLKOUT5_PHASE=0.0, o_CLKOUT5=Signal(name='clkout5'),
            p_CLKOUT6_DIVIDE=15, p_CLKOUT6_PHASE=0.0, o_CLKOUT6=pll_rngclk,
        )
"""
