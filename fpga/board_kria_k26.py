import os
import subprocess

from amaranth.build import *
from amaranth.vendor.xilinx import *

__all__ = ["KriaK26Platform"]

class KriaK26Platform(XilinxPlatform):
    device      = "xck26"
    package     = "sfvc784"
    speed       = "2LV-c"
    default_clk = "clk100"
    resources = []
    connectors = [
        Connector("som", 1, {
            "D18": "J11",  # IO_L1P_AD15P_45
            "B16": "J10",  # IO_L1N_AD15N_45
            "B17": "K13",  # IO_L2P_AD14P_45
            "B18": "K12",  # IO_L2N_AD14N_45
            "C18": "H11",  # IO_L3P_AD13P_45
            "C19": "G10",  # IO_L3N_AD13N_45
            "A16": "J12",  # IO_L4P_AD12P_45
            "A17": "H12",  # IO_L4N_AD12N_45
            "D16": "G11",  # IO_L5P_HDGC_45
            "D17": "F10",  # IO_L5N_HDGC_45
            "C20": "F12",  # IO_L6P_HDGC_45
            "A15": "F11",  # IO_L6N_HDGC_45
            "D20": "E10",  # IO_L7P_HDGC_45
            "D21": "D10",  # IO_L7N_HDGC_45
            "B21": "E12",  # IO_L8P_HDGC_45
            "B22": "D11",  # IO_L8N_HDGC_45
            "D22": "C11",  # IO_L9P_AD11P_45
            "B20": "B10",  # IO_L9N_AD11N_45
            "C22": "B11",  # IO_L10P_AD10P_45
            "C23": "A10",  # IO_L10N_AD10N_45
            "C24": "A12",  # IO_L11P_AD9P_45
            "C3":  "G1",   # IO_L1P_T0L_N0_DBC_66
            "C4":  "F1",   # IO_L1N_T0L_N1_DBC_66
            "D7":  "E1",   # IO_L2P_T0L_N2_66
            "D8":  "D1",   # IO_L2N_T0L_N3_66
            "D4":  "F2",   # IO_L3P_T0L_N4_AD15P_66
            "D5":  "E2",   # IO_L3N_T0L_N5_AD15N_66
            "C6":  "G3",   # IO_L4P_T0U_N6_DBC_AD7P_66
            "C7":  "F3",   # IO_L4N_T0U_N7_DBC_AD7N_66
            "B4":  "E4",   # IO_L5P_T0U_N8_AD14P_66
            "B5":  "E3",   # IO_L5N_T0U_N9_AD14N_66
            "B1":  "C1",   # IO_L7P_T1L_N0_QBC_AD13P_66
            "B2":  "B1",   # IO_L7N_T1L_N1_QBC_AD13N_66
            "A3":  "A2",   # IO_L8P_T1L_N2_AD5P_66
            "A4":  "A1",   # IO_L8N_T1L_N3_AD5N_66
            "B7":  "B3",   # IO_L9P_T1L_N4_AD12P_66
            "B8":  "A3",   # IO_L9N_T1L_N5_AD12N_66
            "C9":  "B4",   # IO_L10P_T1U_N6_QBC_AD4P_66
            "C10": "A4",   # IO_L10N_T1U_N7_QBC_AD4N_66
            "D10": "D4",   # IO_L11P_T1U_N8_GC_66
            "D11": "C4",   # IO_L11N_T1U_N9_GC_66
            "A6":  "C3",   # IO_L12P_T1U_N10_GC_66
            "A7":  "C2",   # IO_L12N_T1U_N11_GC_66
            "C12": "D7",   # IO_L13P_T2L_N0_GC_QBC_66
            "C13": "D6",   # IO_L13N_T2L_N1_GC_QBC_66
            "B10": "E5",   # IO_L14P_T2L_N2_GC_66
            "B11": "D5",   # IO_L14N_T2L_N3_GC_66
            "A9":  "G6",   # IO_L15P_T2L_N4_AD11P_66
            "A10": "F6",   # IO_L15N_T2L_N5_AD11N_66
            "A12": "G8",   # IO_L16P_T2U_N6_QBC_AD3P_66
            "A13": "F7",   # IO_L16N_T2U_N7_QBC_AD3N_66
            "D13": "F8",   # IO_L17P_T2U_N8_AD10P_66
            "D14": "E8",   # IO_L17N_T2U_N9_AD10N_66
        }),

        Connector("som", 2, {
            "B9":  "Y2",   # MGTHRXP0_224
            "D1":  "V2",   # MGTHRXP1_224
            "B1":  "T2",   # MGTHRXP2_224
            "D5":  "P2",   # MGTHRXP3_224
            "B10": "Y1",   # MGTHRXN0_224
            "D2":  "V1",   # MGTHRXN1_224
            "B2":  "T1",   # MGTHRXN2_224
            "D6":  "P1",   # MGTHRXN3_224
            "D9":  "W4",   # MGTHTXP0_224
            "C7":  "U4",   # MGTHTXP1_224
            "B5":  "R4",   # MGTHTXP2_224
            "A3":  "N4",   # MGTHTXP3_224
            "D10": "W3",   # MGTHTXN0_224
            "C8":  "U3",   # MGTHTXN1_224
            "B6":  "R3",   # MGTHTXN2_224
            "A4":  "N3",   # MGTHTXN3_224
            "C3":  "Y6",   # MGTREFCLK0P_224
            "C4":  "Y5",   # MGTREFCLK0N_224
            "A7":  "V6",   # MGTREFCLK1P_224
            "A8":  "V5",   # MGTREFCLK1N_224
            "D46": "AG10", # IO_L1P_AD11P_43
            "D48": "AH10", # IO_L1N_AD11N_43
            "D49": "AF11", # IO_L2P_AD10P_43
            "D50": "AG11", # IO_L2N_AD10N_43
            "C46": "AH12", # IO_L3P_AD9P_43
            "C47": "AH11", # IO_L3N_AD9N_43
            "C51": "AE10", # IO_L4P_AD8P_43
            "C52": "AF10", # IO_L4N_AD8N_43
            "D44": "AE12", # IO_L5P_HDGC_AD7P_43
            "D45": "AF12", # IO_L5N_HDGC_AD7N_43
            "C48": "AC12", # IO_L6P_HDGC_AD6P_43
            "C50": "AD12", # IO_L6N_HDGC_AD6N_43
            "B44": "AD11", # IO_L7P_HDGC_AD5P_43
            "B45": "AD10", # IO_L7N_HDGC_AD5N_43
            "B49": "AB11", # IO_L8P_HDGC_AD4P_43
            "B50": "AC11", # IO_L8N_HDGC_AD4N_43
            "B46": "AA11", # IO_L9P_AD3P_43
            "B48": "AA10", # IO_L9N_AD3N_43
            "A46": "W10",  # IO_L10P_AD2P_43
            "A47": "Y10",  # IO_L10N_AD2N_43
            "A48": "Y9",   # IO_L11P_AD1P_43
            "A50": "AA8",  # IO_L11N_AD1N_43
            "A51": "AB10", # IO_L12P_AD0P_43
            "A52": "AB9",  # IO_L12N_AD0N_43
            "D54": "AE15", # IO_L1P_AD15P_44
            "D56": "AE14", # IO_L1N_AD15N_44
            "D57": "AG14", # IO_L2P_AD14P_44
            "D58": "AH14", # IO_L2N_AD14N_44
            "C54": "AG13", # IO_L3P_AD13P_44
            "C55": "AH13", # IO_L3N_AD13N_44
            "C59": "AE13", # IO_L4P_AD12P_44
            "C60": "AF13", # IO_L4N_AD12N_44
            "D52": "AD15", # IO_L5P_HDGC_44
            "D53": "AD14", # IO_L5N_HDGC_44
            "C56": "AC14", # IO_L6P_HDGC_44
            "C58": "AC13", # IO_L6N_HDGC_44
            "B52": "AA13", # IO_L7P_HDGC_44
            "B53": "AB13", # IO_L7N_HDGC_44
            "B57": "AB15", # IO_L8P_HDGC_44
            "B58": "AB14", # IO_L8N_HDGC_44
            "B54": "W14",  # IO_L9P_AD11P_44
            "B56": "W13",  # IO_L9N_AD11N_44
            "A54": "Y14",  # IO_L10P_AD10P_44
            "A55": "Y13",  # IO_L10N_AD10N_44
            "A56": "W12",  # IO_L11P_AD9P_44
            "A58": "W11",  # IO_L11N_AD9N_44
            "A59": "Y12",  # IO_L12P_AD8P_44
            "A60": "AA12", # IO_L12N_AD8N_44
            "D33": "AC9",  # IO_L1P_T0L_N0_DBC_64
            "D34": "AD9",  # IO_L1N_T0L_N1_DBC_64
            "D30": "AE9",  # IO_L2P_T0L_N2_64
            "D31": "AE8",  # IO_L2N_T0L_N3_64
            "D36": "AB8",  # IO_L3P_T0L_N4_AD15P_64
            "D37": "AC8",  # IO_L3N_T0L_N5_AD15N_64
            "A41": "AD7",  # IO_L4P_T0U_N6_DBC_AD7P_64
            "A42": "AE7",  # IO_L4N_T0U_N7_DBC_AD7N_64
            "D39": "AB7",  # IO_L5P_T0U_N8_AD14P_64
            "D40": "AC7",  # IO_L5N_T0U_N9_AD14N_64
            "C38": "AG9",  # IO_L7P_T1L_N0_QBC_AD13P_64
            "C39": "AH9",  # IO_L7N_T1L_N1_QBC_AD13N_64
            "B27": "AF8",  # IO_L8P_T1L_N2_AD5P_64
            "B28": "AG8",  # IO_L8N_T1L_N3_AD5N_64
            "B36": "AH8",  # IO_L9P_T1L_N4_AD12P_64
            "B37": "AH7",  # IO_L9N_T1L_N5_AD12N_64
            "A29": "AG6",  # IO_L10P_T1U_N6_QBC_AD4P_64
            "A30": "AG5",  # IO_L10N_T1U_N7_QBC_AD4N_64
            "D27": "AF7",  # IO_L11P_T1U_N8_GC_64
            "D28": "AF6",  # IO_L11N_T1U_N9_GC_64
            "C41": "AE5",  # IO_L12P_T1U_N10_GC_64
            "C42": "AF5",  # IO_L12N_T1U_N11_GC_64
            "C29": "AD5",  # IO_L13P_T2L_N0_GC_QBC_64
            "C30": "AD4",  # IO_L13N_T2L_N1_GC_QBC_64
            "C32": "AC4",  # IO_L14P_T2L_N2_GC_64
            "C33": "AC3",  # IO_L14N_T2L_N3_GC_64
            "C35": "AB4",  # IO_L15P_T2L_N4_AD11P_64
            "C36": "AB3",  # IO_L15N_T2L_N5_AD11N_64
            "B30": "AD2",  # IO_L16P_T2U_N6_QBC_AD3P_64
            "B31": "AD1",  # IO_L16N_T2U_N7_QBC_AD3N_64
            "A35": "AB2",  # IO_L17P_T2U_N8_AD10P_64
            "A36": "AC2",  # IO_L17N_T2U_N9_AD10N_64
            "A38": "AG4",  # IO_L19P_T3L_N0_DBC_AD9P_64
            "A39": "AH4",  # IO_L19N_T3L_N1_DBC_AD9N_64
            "B33": "AG3",  # IO_L20P_T3L_N2_AD1P_64
            "B34": "AH3",  # IO_L20N_T3L_N3_AD1N_64
            "C26": "AE3",  # IO_L21P_T3L_N4_AD8P_64
            "C27": "AF3",  # IO_L21N_T3L_N5_AD8N_64
            "B39": "AE2",  # IO_L22P_T3U_N6_DBC_AD0P_64
            "B40": "AF2",  # IO_L22N_T3U_N7_DBC_AD0N_64
            "A32": "AH2",  # IO_L23P_T3U_N8_64
            "A33": "AH1",  # IO_L23N_T3U_N9_64
            "D15": "W8",   # IO_L1P_T0L_N0_DBC_65
            "D16": "Y8",   # IO_L1N_T0L_N1_DBC_65
            "D12": "U9",   # IO_L2P_T0L_N2_65
            "D13": "V9",   # IO_L2N_T0L_N3_65
            "C17": "U8",   # IO_L3P_T0L_N4_AD15P_65
            "C18": "V8",   # IO_L3N_T0L_N5_AD15N_65
            "B24": "R8",   # IO_L4P_T0U_N6_DBC_AD7P_SMBALERT_65
            "B25": "T8",   # IO_L4N_T0U_N7_DBC_AD7N_65
            "D21": "R7",   # IO_L5P_T0U_N8_AD14P_65
            "D22": "T7",   # IO_L5N_T0U_N9_AD14N_65
            "B18": "L1",   # IO_L7P_T1L_N0_QBC_AD13P_65
            "B19": "K1",   # IO_L7N_T1L_N1_QBC_AD13N_65
            "A20": "J1",   # IO_L8P_T1L_N2_AD5P_65
            "A21": "H1",   # IO_L8N_T1L_N3_AD5N_65
            "B15": "K2",   # IO_L9P_T1L_N4_AD12P_65
            "B16": "J2",   # IO_L9N_T1L_N5_AD12N_65
            "A14": "H4",   # IO_L10P_T1U_N6_QBC_AD4P_65
            "A15": "H3",   # IO_L10N_T1U_N7_QBC_AD4N_65
            "C11": "K4",   # IO_L11P_T1U_N8_GC_65
            "C12": "K3",   # IO_L11N_T1U_N9_GC_65
            "D18": "L3",   # IO_L12P_T1U_N10_GC_65
            "D19": "L2",   # IO_L12N_T1U_N11_GC_65
            "B12": "L7",   # IO_L13P_T2L_N0_GC_QBC_65
            "B13": "L6",   # IO_L13N_T2L_N1_GC_QBC_65
            "B21": "M6",   # IO_L14P_T2L_N2_GC_65
            "B22": "L5",   # IO_L14N_T2L_N3_GC_65
            "A17": "N7",   # IO_L15P_T2L_N4_AD11P_65
            "A18": "N6",   # IO_L15N_T2L_N5_AD11N_65
            "C20": "P7",   # IO_L16P_T2U_N6_QBC_AD3P_65
            "C21": "P6",   # IO_L16N_T2U_N7_QBC_AD3N_65
            "C14": "N9",   # IO_L17P_T2U_N8_AD10P_65
            "C15": "N8",   # IO_L17N_T2U_N9_AD10N_65
            "A11": "J5",   # IO_L19P_T3L_N0_DBC_AD9P_65
            "A12": "J4",   # IO_L19N_T3L_N1_DBC_AD9N_65
            "A23": "J7",   # IO_L21P_T3L_N4_AD8P_65
            "A24": "H7",   # IO_L21N_T3L_N5_AD8N_65
            "D24": "K8",   # IO_L22P_T3U_N6_DBC_AD0P_65
            "D25": "K7",   # IO_L22N_T3U_N7_DBC_AD0N_65
            "C23": "K9",   # IO_L23P_T3U_N8_I2C_SCLK_65
            "C24": "J9",   # IO_L23N_T3U_N9_65
            "A26": "H9",   # IO_L24P_T3U_N10_PERSTN1_I2C_SDA_65
            "A27": "H8",   # IO_L24N_T3U_N11_PERSTN0_65
        })
    ]

    constraints = "set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]\n"
    constraints += "set_operating_conditions -ambient_temp 55.0\n"
    tcl_after_read = "set_property board_part xilinx.com:k26c:part0:1.3 [current_project]\n"
    tcl_after_synth = r"""
                      foreach cell [get_cells -quiet -hier -filter {hi.false_path != ""}] {
                          set_false_path -from [get_property hi.false_path $cell] -to $cell
                      }
                      save_design
                      """

    def toolchain_prepare(self, fragment, name, **kwargs):
        overrides = {
            "script_after_read": self.tcl_after_read,
            "add_constraints": self.constraints,
            "script_after_synth": self.tcl_after_synth,
            #"script_after_bitstream":"\n",
        }
        return super().toolchain_prepare(fragment, name, **overrides, **kwargs)

    def toolchain_program(self, products, name):
        openocd = os.environ.get("OPENOCD", "openocd")
        with products.extract("{}.bit".format(name)) as bitstream_filename:
            subprocess.check_call([openocd,
                "-c", "source [find board/kcu105.cfg]; init; pld load 0 {}; exit"
                      .format(bitstream_filename)
            ])
