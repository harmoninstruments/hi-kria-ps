# Copyright 2022 Harmon Instruments, LLC
# MIT license
# generate Amaranth HDL connector Python from JSON pin lists

import shlex
import json

def remap_sort(x):
    x = x.split("_")
    bank = x[-1]
    if x[0] == "IO":
        x = x[1:]
    pin = x[0]
    pol = 0
    if pin[-1] == "P":
        pol = 1;
        pin = pin[:-1]
    if pin[-1] == "N":
        pol = 2;
        pin = pin[:-1]
    pin = pin.replace("P", "a").replace("N", "b")
    if pin[0] == "L":
        pin = int(pin[1:])
    elif "MGT" in pin:
        pin = pin
    rv = [bank, pin, pol]
    return rv

def print_connector(pins, n=0):
    rv = f'    Connector("som", {n},' + ' {\n'
    pins_sorted = sorted(pins.items(), key=lambda x: remap_sort(x[1][1]))
    for pin in pins_sorted:
        conn_pin = '"'+pin[0]+'":'
        if len(conn_pin) < 6:
            conn_pin += " "*(6-len(conn_pin))
        fpga_pin = '"'+pin[1][0]+'",'
        if len(fpga_pin) < 7:
            fpga_pin += " "*(7-len(fpga_pin))
        rv += f'        {conn_pin} {fpga_pin} # {pin[1][1]}\n'
    rv += '    })\n'
    return rv

if __name__ == '__main__':
    for i in [1,2]:
        with open(f"../kria_pins/som{i}.json", "r") as f:
            som = json.load(f)
            print(print_connector(som, i))
