#!/usr/bin/env python3
import os, sys
import subprocess

sys.path.append("harmon-instruments-open-hdl/")
sys.path.append("amaranth-soc")

from amaranth.build import *
from amaranth import *
from amaranth.lib.cdc import FFSynchronizer

from board_kria_k26 import KriaK26Platform

from AXI.Bus import AXI4Bus
from PIO.AXI import PIO_AXI
from ClockGen import ClockGen
from CDC.CDC import CDC_Gray

attrs_18_slow = Attrs(IOSTANDARD="LVCMOS18", SLEW="SLOW", DRIVE="4")
attrs_33_slow = Attrs(IOSTANDARD="LVCMOS33", SLEW="SLOW", DRIVE="4")
attrs_33_pu = Attrs(IOSTANDARD="LVCMOS33", PULLUP="TRUE", DRIVE="4")

hi_ps = [
    # IO_L12P_T1U_N10_GC_66 IO_L12N_T1U_N11_GC_66
    Resource("clk100", 0, DiffPairs("A6", "A7", dir="i", conn=("som", 1)),
             Clock(100e6), Attrs(IOSTANDARD="LVDS", DIFF_TERM_ADV="TERM_100")),
    Resource("led", 0, Pins("D18", dir='o', conn=("som", 1)), attrs_33_slow),
    Resource("gps", 0,
             Subsignal("en", Pins("D16", dir='o', conn=("som", 1)), attrs_33_slow),
             Subsignal("pps", Pins("D17", dir='i', conn=("som", 1)),
                       Attrs(IOSTANDARD="LVCMOS33", PULLDOWN="TRUE")),
             # RX and TX are from GPS perspective
             Subsignal("rxd", Pins("D20", dir='o', conn=("som", 1)), attrs_33_slow),
             Subsignal("txd", Pins("D21", dir='i', conn=("som", 1)),
                       Attrs(IOSTANDARD="LVCMOS33", PULLUP="TRUE")),
             ),
    Resource("clock_adc", 0,
             Subsignal("sck", Pins("B18", dir='o', conn=("som", 1)), attrs_33_slow),
             Subsignal("copi", Pins("C18", dir='o', conn=("som", 1)), attrs_33_slow),
             Subsignal("cs", Pins("C19", dir='o', conn=("som", 1)), attrs_33_slow),
             Subsignal("mclk", Pins("A15", dir='o', conn=("som", 1)), attrs_33_slow),
             Subsignal("mdat", Pins("A16", dir='i', conn=("som", 1)), attrs_33_pu),
             Subsignal("cipo", Pins("A17", dir='i', conn=("som", 1)), attrs_33_pu),
             ),
    Resource("swd", 0,
             Subsignal("clk", Pins("A3", dir='o', conn=("som", 1)), attrs_18_slow),
             Subsignal("io", Pins("A4", dir='io', conn=("som", 1)), attrs_18_slow),
             ),
    Resource("clockfpga", 0,
             Subsignal("zynq_ice",
                       Pins("A9", dir='o', conn=("som", 1)), # A9 L15P_T2L_N4_AD11P_66
                       Attrs(IOSTANDARD="HSTL_18_I")),
             Subsignal("ice_zynq",
                       Pins("A10", dir='i', conn=("som", 1)), # L15N_T2L_N5_AD11N_66
                       Attrs(IOSTANDARD="HSTL_18_I")),
             ),
    # D13 L17P_T2U_N8_AD10P_66 D14 L17N_T2U_N9_AD10N_66 C12 L13P_T2L_N0_GC_QBC_66
    Resource("ufl", 0, Pins("D13 D14 C12", dir='o', conn = ("som", 1)),
             Attrs(IOSTANDARD="HSTL_I_18", SLEW="FAST", OUTPUT_IMPEDANCE="RDRV_48_48")),
    Resource("i2c_display", 0,
             Subsignal("scl", Pins("B18", dir='io', conn=("som", 1)), attrs_33_slow),
             Subsignal("sda", Pins("C20", dir='io', conn=("som", 1)), attrs_33_slow),
             Subsignal("int", Pins("C23", dir='i', conn=("som", 1)), attrs_33_pu),
             ),
]

axi_pio0 = AXI4Bus("PIO0_", abits=40, dbits=128, idbits=16, lenbits=8, lockbits=1, sizebits=3)

class PPS_timestamper(Elaboratable):
    """
    Generate timestamps of the rising edge of a PPS signal from a GPS
    unit. Uses a DDR input to double the effective sample rate

    assumes sync clock domain is 125 MHz, c500 is 500 MHz, they are
    safely timed together

    HDIO IDDR is limited to 125 MHz clock, so better to do it this way

    the value read from the bus is a 64 bit timestamp of the last rising
    edge in units of nanoseconds since FPGA configuration

    pio: PIO_Generic class, the bus interface, needs to be at least 64 bits

    addr: base address on bus

    pin: the input pin, buffered outside this module as we may also want
    to route it to a PS EMIO GPIO

    count: shared free runing 500 MHz counter, 63 bits

    """
    def __init__(self, pio, addr, count, pin):
        self.count = count # 62 bits, 4 ns units
        self.pin = pin
        self.odata = Signal(63) # 2 ns units
        pio.add_ro_reg(addr, Cat(C(0), self.odata, C(0,64)))
    def elaborate(self, plaform):
        m = Module()
        in_s = Signal()
        m.submodules.sync_in = FFSynchronizer(i=self.pin, o=in_s, o_domain='c500')
        in_sreg = Signal(2)
        m.d.c500 += in_sreg.eq(Cat(in_s, in_sreg[0]))
        count_capture_500 = Signal(63)
        with m.If(in_sreg == 0b01): # rising edge
            m.d.c500 += count_capture_500.eq(self.count)
        m.d.sync += self.odata.eq(count_capture_500)

        return m

class HIKriaPS(Elaboratable):
    def elaborate(self, platform):
        m = Module()
        m.domains.sync = ClockDomain("sync", reset_less=True) # 125 MHz main clock
        m.domains.c250 = ClockDomain("c250", reset_less=True) # 250 MHz
        m.domains.c500 = ClockDomain("c500", reset_less=True) # 500 MHz
        m.domains.c625 = ClockDomain("c625", reset_less=True) # 625 MHz

        clockgen = m.submodules.clockgen = ClockGen()

        led = platform.request("led", 0)
        gps = platform.request("gps", 0)
        i2c0 = platform.request("i2c_display", 0)
        count500 = Signal(63)
        count250 = Signal(62)
        count125 = Signal(61)
        m.d.c500 += count500.eq(count500 + 1)
        m.d.c250 += count250.eq(count500[1:])
        m.d.sync += count125.eq(count250[1:])
        m.d.comb += led.eq(count125[25]),
        m.d.sync += gps.en.eq(1)

        #wire [7:0]pl_ps_irq0_0;

        scl_t = Signal()
        sda_t = Signal()
        m.d.comb += [
            i2c0.scl.oe.eq(~scl_t),
            i2c0.sda.oe.eq(~sda_t),
        ]

        m.submodules.bd = Instance(
            "kria_bd",
            i_maxihpm0_fpd_aclk_0 = ClockSignal(),
            i_IIC_0_0_scl_i = i2c0.scl.i,
            o_IIC_0_0_scl_o = i2c0.scl.o,
            o_IIC_0_0_scl_t = scl_t,
            i_IIC_0_0_sda_i = i2c0.sda.i,
            o_IIC_0_0_sda_o = i2c0.sda.o,
            o_IIC_0_0_sda_t = sda_t,
            i_UART_1_0_rxd = gps.txd,
            o_UART_1_0_txd = gps.rxd,
            i_pl_ps_irq0_0 = C(0,8),
            **axi_pio0.get_zynq_ports("M_AXI_HPM0_FPD_0_"),
        )

        pio = m.submodules.pio0 = PIO_AXI(axi_pio0)

        pio.add_ro_reg(0xA0000000, C(0xABCD0123DEADBEEFCAFED00D))
        r10 = Signal(128)
        pio.add_rw_reg(0xA0000010, r10)
        pio.add_ro_reg(0xA0000020, Cat(C(0,3), count125, C(0,64)))
        m.submodules.pps_timestamper = PPS_timestamper(pio, 0xA0000030, count500, gps.pps)
        pio.add_ro_reg(0xA1000000, pio.raddr, lsb=20)

        count_625 = Signal(16, reset=0xBEEF)

        cdc = m.submodules.cdc_625 = CDC_Gray(
            i=count_625, cin='c625', cout='sync', max_input_delay=2e-9)
        m.d.c625 += count_625.eq(count_625 + 1)
        pio.add_ro_reg(0xA0000040, cdc.o)

        # U.FLs
        ufl = platform.request("ufl", 0, dir='-')

        # 25 MHz + ODDR on UFL2
        count_625_25 = Signal(5)
        count_625_25_next = Signal(5)
        m.d.comb += count_625_25_next.eq(Mux(count_625_25 == 24, C(0), count_625_25 + 1))
        m.d.c625 += count_625_25.eq(count_625_25_next)
        ufl2 = Signal()
        m.d.c625 += ufl2.eq(count_625_25[4])

        ufl2_oddr = Signal()
        m.submodules.oddr_ufl2 = Instance(
            "ODDRE1",
            o_Q=ufl2_oddr,
            i_C=ClockSignal("c625"),
            i_D1=ufl2,
            i_D2=ufl2,
            i_SR=C(0)
        )

        ufl2_odelay_out = Signal()
        ufl2_odelay_ce = Signal()
        ufl2_odelay_val = Signal(9)

        w_ufl2d = pio.add_wo_reg(0xA0000050, ufl2_odelay_val)
        m.d.sync += ufl2_odelay_ce.eq(w_ufl2d)

        m.submodules.odelay_ufl2 = Instance(
            "ODELAYE3",
            p_CASCADE ="NONE",
            p_DELAY_FORMAT="COUNT",
            p_DELAY_TYPE="VAR_LOAD",
            p_DELAY_VALUE=0,
            p_UPDATE_MODE="ASYNC",
            p_SIM_DEVICE="ULTRASCALE_PLUS",
            o_CASC_OUT=Signal(),
            o_CNTVALUEOUT=Signal(9),
            o_DATAOUT=ufl2_odelay_out,
            i_CASC_IN=C(0),
            i_CASC_RETURN=C(0),
            i_CE=C(0),
            i_CLK=ClockSignal(),
            i_CNTVALUEIN=ufl2_odelay_val,
            i_EN_VTC=C(0),
            i_INC=C(0),
            i_LOAD=ufl2_odelay_ce,
            i_ODATAIN=ufl2_oddr,
            i_RST=clockgen.reset,
        )

        ufl2_iobuf_out = Signal()
        m.submodules.iobuf_ufl2 = Instance(
            "IOBUF",
            o_O=ufl2_iobuf_out,
            i_I=ufl2_odelay_out,
            io_IO=ufl[2],
            i_T=C(0),
        )

        ufl2_iddr = Signal(2)
        m.submodules.iddr_ufl2 = Instance(
            "IDDRE1",
            p_DDR_CLK_EDGE="SAME_EDGE_PIPELINED",
            p_IS_CB_INVERTED=1,
            o_Q1=ufl2_iddr[0],
            o_Q2=ufl2_iddr[1],
            i_C=ClockSignal("c500"),
            i_CB=ClockSignal("c500"),
            i_D=ufl2_iobuf_out,
            i_R=C(0),
        )

        count_ufl2i = Signal(5)
        ufl2isr = Signal(40)
        ufl2icap = Signal(40)
        trig = Signal()
        m.d.c500 += [
            count_ufl2i.eq(Mux(count_ufl2i == 19, C(0), count_ufl2i + 1)),
            ufl2isr.eq(Cat(ufl2_iddr, ufl2isr[:-2])),
            trig.eq(count_ufl2i == 0),
        ]
        with m.If(trig):
            m.d.c500 += ufl2icap.eq(ufl2isr)
        ufl2icapsync = Signal(40)
        m.d.sync += ufl2icapsync.eq(ufl2icap)

        pio.add_ro_reg(0xA0000060, ufl2icapsync)

        return m

if __name__ == "__main__":
    p = KriaK26Platform()
    p.tcl_after_read += "add_files -norecurse ../build_cores/hi-kria-cores.srcs/sources_1/bd/kria_bd/kria_bd.bd\n"
    #p.tcl_after_read += "launch_runs synth_1 -jobs 12"
    #p.tcl_after_synth += "launch_runs impl_1 -jobs 12"
    p.constraints += "set_property INTERNAL_VREF 0.9 [get_iobanks 64]\n"
    p.constraints += "set_property INTERNAL_VREF 0.9 [get_iobanks 65]\n"
    p.constraints += "set_property INTERNAL_VREF 0.9 [get_iobanks 66]\n"
    p.add_resources(hi_ps)
    p.build(
        HIKriaPS(),
        name='hi_kria',
        do_program=False,
        verbose=True,
        do_build=True)
