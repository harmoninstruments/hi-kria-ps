create_project hi-kria-cores build_cores -part xck26-sfvc784-2LV-c
set_property board_part xilinx.com:k26c:part0:1.3 [current_project]
create_bd_design "kria_bd"
update_compile_order -fileset sources_1
startgroup
create_bd_cell -type ip -vlnv xilinx.com:ip:zynq_ultra_ps_e:3.4 zynq_ultra_ps_e_0
endgroup
apply_bd_automation -rule xilinx.com:bd_rule:zynq_ultra_ps_e -config {apply_board_preset "1" }  [get_bd_cells zynq_ultra_ps_e_0]

set_property -dict [list \
                        CONFIG.PSU__PSS_ALT_REF_CLK__FREQMHZ {50.0} \
                        CONFIG.PSU__VIDEO_REF_CLK__ENABLE {0} \
                        CONFIG.PSU__PSS_ALT_REF_CLK__ENABLE {1} \
                        CONFIG.PSU__ENET1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__ENET1__GRP_MDIO__ENABLE {1} \
                        CONFIG.PSU__I2C0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__I2C0__PERIPHERAL__IO {EMIO} \
                        CONFIG.PSU__USB0__REF_CLK_SEL {Ref Clk1} \
                        CONFIG.PSU__USB1__REF_CLK_SEL {Ref Clk1} \
                        CONFIG.PSU__USB0__REF_CLK_FREQ {100} \
                        CONFIG.PSU__USB1__REF_CLK_FREQ {100} \
                        CONFIG.PSU__DP__LANE_SEL {Single Lower} \
                        CONFIG.PSU__UART0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__UART0__PERIPHERAL__IO {MIO 50 .. 51} \
                        CONFIG.PSU__UART1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__UART1__PERIPHERAL__IO {EMIO} \
                        CONFIG.PSU__USB0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__USB__RESET__MODE {Disable} \
                        CONFIG.PSU__USB1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__USB3_0__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__USB3_0__PERIPHERAL__IO {GT Lane2} \
                        CONFIG.PSU__USB3_1__PERIPHERAL__ENABLE {1} \
                        CONFIG.PSU__FPGA_PL0_ENABLE {0} \
                        CONFIG.PSU__FPGA_PL1_ENABLE {0} \
                        CONFIG.PSU__USE__FABRIC__RST {0} \
                        CONFIG.PSU__SD0__PERIPHERAL__ENABLE {1}  \
                        CONFIG.PSU__SD0__PERIPHERAL__IO {MIO 13 .. 22}  \
                        CONFIG.PSU__SD0__GRP_CD__ENABLE {0}  \
                        CONFIG.PSU__SD0__GRP_POW__ENABLE {1}  \
                        CONFIG.PSU__SD0__GRP_POW__IO {MIO 23}  \
                        CONFIG.PSU__SD0__GRP_WP__ENABLE {0}  \
                        CONFIG.PSU__SD0__SLOT_TYPE {eMMC}  \
                        CONFIG.PSU__SD0__RESET__ENABLE {1}  \
                        CONFIG.PSU__SD0__DATA_TRANSFER_MODE {8Bit}  \
                        CONFIG.PSU__DDRC__CWL {16} \
                        CONFIG.PSU_MIO_70_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_70_SLEW {slow} \
                        CONFIG.PSU_MIO_71_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_71_SLEW {slow} \
                        CONFIG.PSU_MIO_72_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_72_SLEW {slow} \
                        CONFIG.PSU_MIO_73_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_73_SLEW {slow} \
                        CONFIG.PSU_MIO_74_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_74_SLEW {slow} \
                        CONFIG.PSU_MIO_75_DRIVE_STRENGTH {4} \
                        CONFIG.PSU_MIO_75_SLEW {slow} \
                        CONFIG.PSU__DISPLAYPORT__PERIPHERAL__ENABLE {0} \
                        CONFIG.PSU__USE__M_AXI_GP0 {1} \
                        CONFIG.PSU__USE__M_AXI_GP1 {0} \
                        CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__SRCSEL {DPLL} \
                        CONFIG.PSU__CRF_APB__TOPSW_MAIN_CTRL__SRCSEL {VPLL} \
                        CONFIG.PSU__CRL_APB__CPU_R5_CTRL__SRCSEL {IOPLL} \
                        CONFIG.PSU__CRF_APB__ACPU_CTRL__FREQMHZ {1325} \
                        CONFIG.PSU__CRF_APB__GPU_REF_CTRL__FREQMHZ {100} \
                        CONFIG.PSU__CRF_APB__DPDMA_REF_CTRL__FREQMHZ {300} \
                        CONFIG.PSU__CRF_APB__APLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRF_APB__DPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRF_APB__VPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRL_APB__IOPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK} \
                        CONFIG.PSU__CRL_APB__RPLL_CTRL__SRCSEL {PSS_ALT_REF_CLK}] [get_bd_cells zynq_ultra_ps_e_0]

make_bd_intf_pins_external [get_bd_intf_pins zynq_ultra_ps_e_0/UART_1]
make_bd_intf_pins_external  [get_bd_intf_pins zynq_ultra_ps_e_0/IIC_0]
make_bd_intf_pins_external  [get_bd_intf_pins zynq_ultra_ps_e_0/M_AXI_HPM0_FPD]
make_bd_pins_external  [get_bd_pins zynq_ultra_ps_e_0/maxihpm0_fpd_aclk]
make_bd_pins_external  [get_bd_pins zynq_ultra_ps_e_0/pl_ps_irq0]

set_property -dict [list CONFIG.PortWidth {8}] [get_bd_ports pl_ps_irq0_0]

assign_bd_address -target_address_space /zynq_ultra_ps_e_0/Data [get_bd_addr_segs M_AXI_HPM0_FPD_0/Reg] -force
# this doesn't seem to matter except for generated headers. All ranges listed in the TRM work.
set_property range 256M [get_bd_addr_segs {zynq_ultra_ps_e_0/Data/SEG_M_AXI_HPM0_FPD_0_Reg}]
set_property offset 0x00A0000000 [get_bd_addr_segs {zynq_ultra_ps_e_0/Data/SEG_M_AXI_HPM0_FPD_0_Reg}]

save_bd_design

generate_target all [get_files kria_bd.bd]

catch { config_ip_cache -export [get_ips -all kria_bd_zynq_ultra_ps_e_0_0] }
export_ip_user_files -of_objects [get_files kria_bd.bd] -no_script -sync -force -quiet
create_ip_run [get_files -of_objects [get_fileset sources_1] kria_bd.bd]
launch_runs kria_bd_zynq_ultra_ps_e_0_0_synth_1 -jobs 12
write_hw_platform -fixed -force -file hi_kria.xsa
wait_on_runs kria_bd_zynq_ultra_ps_e_0_0_synth_1

exit
