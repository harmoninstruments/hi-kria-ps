#!/usr/bin/env python3

import sys, math
sys.path.append("/home/dlharmon/software/kicad/build/pcbnew")
from pcbnew import *

pcb = LoadBoard('hi-kria-pl.kicad_pcb')

lengths = {}
# adjust ns to mm given the propagation velocity on our inner layers
ns_to_mm = 1e-9 * 2.998e8 * 1000.0 / math.sqrt(3.66)
print(ns_to_mm)

# not included in repo due to copyright, search Xilinx XTP688 for this file
# https://www.xilinx.com/cgi-bin/docs/ctdoc?cid=15c61a8b-7bab-4392-b2b3-4c82f12dad3d;d=xtp688-kria-k26-trace-delay.zip
with open('Kria_K26_Trace_Delay.csv') as f:
    for l in f:
        d = l.split(',')
        if not 'JA2' in d[0]:
            continue
        name = f"{d[0][4]}{int(d[0][5:]):02d}"
        # nanoseconds
        lengths[name] = ns_to_mm * (float(d[2]) + float(d[3])) * 0.5

print(lengths)

mod = pcb.FindFootprintByReference('SOM1')
pads = mod.Pads()

for pad in pads:
    name = pad.GetPadName()
    if name in lengths:
        pad.SetPadToDieLength(FromMM(lengths[name]))

pcb.Save("./mod.kicad_pcb")
