#!/usr/bin/env python3

import sys
import math
from pcbnew import *

eri = 3.66 # relative permittivity (inner layer)
ero = 2.6 # relative permittivity (outer layer)
mm_per_m = 1000.0
s_per_ps = 1e-12
c = 2.998e8
scale_inner = 1.0/(c*mm_per_m*s_per_ps/math.sqrt(eri)) # ps per mm
scale_outer = 1.0/(c*mm_per_m*s_per_ps/math.sqrt(ero)) # ps per mm

pcb = LoadBoard('hi-kria-ps.kicad_pcb')
tracks = pcb.GetTracks()
td = {}
pads = pcb.FindFootprintByReference('SOM1').Pads()
for pad in pads:
    name = pad.GetNetname()
    length = ToMM(pad.GetPadToDieLength()) * scale_inner
    td[name] = length

for track in tracks:
    name = track.GetNetname()
    length = ToMM(track.GetLength())
    if(track.GetLayerName() not in ['F.Cu','B.Cu']):
        length *= scale_inner
    else:
        length *= scale_outer
    if name in td:
        td[name] += length
    else:
        td[name] = length

usb = ['CLK', 'D0', 'D1', 'D2', 'D3', 'D4', 'D5', 'D6', 'D7', 'STP', 'NXT', 'DIR']
etx = ['_CLK', 'D0', 'D1', 'D2', 'D3', 'CTL']
dpairs = ['C+', '0+', '1+', '2+', '3+']
extpairs = ['C+', '1+', '2+', '3+']

def plist(nlist, prefix=''):
    minl = 10000
    maxl = 0
    ref = td[prefix+nlist[0]]
    print("ref =", prefix+nlist[0], round(ref,1))
    for net in nlist:
        delay = round(td[prefix+net] - ref,1)
        if net[-1] == '+':
            nnet = net[:-1] + '-'
            ndelay = round(td[prefix+nnet] - ref,1)
            print(net, delay, ndelay)
            maxl = max(maxl, ndelay)
            minl = min(minl, ndelay)
        else:
            print(net, delay)
        maxl = max(maxl, delay)
        minl = min(minl, delay)
    print(maxl, minl)

print("\nEnet TX +- 50 ps")
plist(etx, prefix='/mio/ETH1_TX')
print("\nEnet RX +- 50 ps")
plist(etx, prefix='/mio/ETH1_RX')
print("\nUSB0 < 1.3 ns, +- 100 ps")
plist(usb, prefix='/mio/USB0_')
print("\nUSB1 < 1.3 ns, +- 100 ps")
plist(usb, prefix='/mio/USB1_')
print("\ndisplay")
plist(dpairs, prefix='/DISP')
print("\next")
plist(extpairs, prefix='/EXT')
