lib_name = "kria_k26"

def match_pin(pin, matches):
    for match in matches:
        if match == pin[1][:len(match)]:
            return True
    return False

# fix swapped P and N pairs, put VCCx first
def resort_pn(pins):
    vcc = None
    for i in range(len(pins)-1):
        if vcc is None:
            if pins[i][1][:3] == 'VCC':
                vcc = i
        if pins[i][1][-2:] != '_N':
            continue
        if pins[i+1][1][-2:] != '_P':
            continue
        if pins[i][1][:-2] != pins[i+1][1][:-2]:
            continue
        pins[i], pins[i+1] = pins[i+1], pins[i]
    if vcc is None:
        return pins
    return pins[vcc:] + pins[:vcc]

def gen_sym(f, side=1):
    pins = {}
    subconnectors = ["A", "B", "C", "D"]
    curr_connector = None
    with open(f"pins_{side}.txt", "r") as fin:
        for line in fin:
            l = line.split("#")[0].strip()
            if len(l) == 0:
                continue
            l = l.split(" ")
            assert len(l) == (len(subconnectors) + 1)
            for (sc, pin_name) in zip(subconnectors, l[1:]):
                num = l[0]
                if len(num) == 1:
                    num = "0"+num
                pins[f"{sc}{num}"] = pin_name
    comp_name = f"Kria_K26_{side}"
    print(f'  (symbol "{lib_name}:{comp_name}" (in_bom yes) (on_board yes)', file=f)
    print(f'    (property "Reference" "SOM" (id 0) (at 0 1.27 0)', file=f)
    print(f'      (effects (font (size 1.27 1.27)))', file=f)
    print(f'    )', file=f)
    print(f'    (property "Value" "{lib_name}" (id 1) (at 0 3.81 0)', file=f)
    print(f'      (effects (font (size 1.27 1.27)))', file=f)
    print(f'    )', file=f)
    print(f'    (property "Footprint" "kicad_pcb:Samtec_ADM6-60-01.5-L-4-2-A" (id 2) (at 0 0 0)', file=f)
    print(f'      (effects (font (size 1.27 1.27)) hide)', file=f)
    print(f'    )', file=f)
    print(f'    (property "Datasheet" "" (id 3) (at 0 0 0)', file=f)
    print(f'      (effects (font (size 1.27 1.27)) hide)', file=f)
    print(f'    )', file=f)
    def do_unit(match, i):
        pin_x = -20.32
        pin_y = -2.54
        pin_dy = -2.54
        pin_len = 5.08
        rv = f'    (symbol "{comp_name}_{i}_1"\n'
        pins_sorted = sorted(pins.items(), key=lambda x: x[1])
        pins_sorted = resort_pn(pins_sorted)
        for pin in pins_sorted:
            if not match_pin(pin, match):
                continue
            pins.pop(pin[0])
            rv += f'      (pin passive line (at {pin_x:.2f} {pin_y:.2f} 0) (length {pin_len})\n'
            rv += f'        (name "{pin[1]}" (effects (font (size 1.27 1.27))))\n'
            rv += f'        (number "{pin[0]}" (effects (font (size 1.27 1.27))))\n'
            rv += f'      )\n'
            pin_y += pin_dy
        rv += f'       (rectangle (start {pin_x + pin_len:.2f} 0) (end {-(pin_x + pin_len):.2f} {pin_y:.2f})\n'
        rv += f'         (stroke (width 0.1524)) (fill (type background))\n'
        rv += f'       )\n'
        rv += f'    )\n'
        return rv
    print(do_unit(["GND", "VCC_SOM"],1), file=f)
    print(do_unit(["GT"],2), file=f)
    if side == '1':
        print(do_unit(["HPA", "VCCO_HPA"],3), file=f)
        print(do_unit(["HDA", "VCCO_HDA"],4), file=f)
        print(do_unit(["MIO"],5), file=f)
        print(do_unit([""],6), file=f)
    if side == '2':
        print(do_unit(["HDB", "VCCO_HDB"],3), file=f)
        print(do_unit(["HDC", "VCCO_HDC"],4), file=f)
        print(do_unit(["HPB", "VCCO_HPB"],5), file=f)
        print(do_unit(["HPC", "VCCO_HPC"],6), file=f)
        #print(do_unit([""],7), file=f)

    print(f'  )', file=f)

if __name__ == '__main__':
    with open(f"../{lib_name}.kicad_sym", "w") as f:
        print(f'(kicad_symbol_lib (version 20201005) (generator kria_k26_import)', file=f)
        gen_sym(f, side='1')
        gen_sym(f, side='2')
        print(f')', file=f)
