#!/usr/bin/env python3
# Copyright 2022 Harmon Instruments, LLC
# MIT license
# sets pad to die length in Kicad for the SOM connector pins
# includes SOM PCB, SOC package delays
# converts delay to length using propagation velocity of OSHPark 6 layer inner

import sys, math, json
sys.path.append("/home/dlharmon/software/kicad/build/pcbnew")
from pcbnew import *

with open("../kria_pins/som_delays_JA1.json", "r") as f:
    som_delays = json.load(f)

# adjust picoseconds to mm given the propagation velocity on our inner layers
ps_to_mm = 1e-12 * 2.998e8 * 1000.0 / math.sqrt(3.66)

pcb = LoadBoard('hi-kria-ps.kicad_pcb')
mod = pcb.FindFootprintByReference('SOM1')
pads = mod.Pads()

for pad in pads:
    name = pad.GetPadName()
    if name in som_delays:
        len_mm = round(som_delays[name]*ps_to_mm,3)
        pad.SetPadToDieLength(FromMM(len_mm))

pcb.Save("./mod.kicad_pcb")
