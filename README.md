# hi-kria-ps

Test and measurement platform based on the Xilinx Kria K26 SOM. This only interfaces with the PS side connector.

## Power Manager and Configuration Controller

An RP2040 microcontrol provides this functionality.

### Xilinx Virtual Cable

### Building The Firmware
 Dependencies:
  - Pico-SDK
  - Python 3.x
  - Amaranth HDL
  - NextPNR for ICE40
  - ...

## Clock Generation

### Notes

DDR4 is MT40A512M16LY-062E:E
QSPI is MT25QU512ABB8E12-0SIT, this is not specified in the Kria K26C docs. 100k erase cycles per sector.
eMMC is MTFC16GAP according to device tree provided by Xilinx

## License
Multiple, details TBD
