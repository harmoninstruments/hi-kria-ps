#!/bin/bash
SCRIPT_DIR=$(cd $(dirname "${BASH_SOURCE[0]}") && pwd)
export REPO_TOP=$SCRIPT_DIR
export DL_DIR=$REPO_TOP/boot/dl/
export SOURCE_DATE_EPOCH=$(git log -1 --pretty=%ct)
TC_DIR=$REPO_TOP/toolchains
export PATH=$PATH:$REPO_TOP/tools/

setup_aarch64_none() {
    DIR=$TC_DIR/aarch64-none
    export PATH=$PATH:$DIR/bin/
    export CROSS_COMPILE=$DIR/bin/aarch64-none-elf-
    export ARCH=arm64
    mkdir -p $DIR
    FILE=$DL_DIR/gcc-arm-11.2-2022.02-x86_64-aarch64-none-elf.tar.xz
    URL='https://developer.arm.com/-/media/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-aarch64-none-elf.tar.xz'
    if [ ! -f $FILE ]; then
        echo "downloading toolchain"
        mkdir -p $DL_DIR
        download.py --url $URL --file $FILE \
                    --sha b0a015a9e8cbb44ed2fe5ad755a7a7ae254d54f93df3bf47378485b0ba8b828b
    fi

    if [ ! -f ${CROSS_COMPILE}gcc ]; then
        echo "extracting toolchain"
        tar xf $FILE -C $DIR --strip-components 1
    fi

}

setup_arm32() {
    DIR=$TC_DIR/arm32-none
    export CROSS_COMPILE=$DIR/bin/arm-none-eabi-
    export ARCH=arm
    mkdir -p $DIR
    FILE=$DL_DIR/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi.tar.xz
    URL='https://developer.arm.com/-/media/Files/downloads/gnu/11.2-2022.02/binrel/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi.tar.xz'
    if [ ! -f $FILE ]; then
        echo "downloading toolchain"
        mkdir -p $DL_DIR
        download.py --url $URL --file $FILE \
                    --sha 8c5acd5ae567c0100245b0556941c237369f210bceb196edfe5a2e7532c60326
    fi

    if [ ! -f ${CROSS_COMPILE}gcc ]; then
        echo "extracting toolchain"
        tar xf $FILE -C $DIR --strip-components 1
    fi
    export PATH=$PATH:$DIR/bin/
}

setup_oss-cad-suite() {
    DIR=$TC_DIR/oss-cad-suite
    FILE=$DL_DIR/oss-cad-suite-linux-x64-20220510.tgz
    URL='https://github.com/YosysHQ/oss-cad-suite-build/releases/download/2022-05-10/oss-cad-suite-linux-x64-20220510.tgz'
    if [ ! -f $FILE ]; then
        echo "downloading oss-cad-suite"
        mkdir -p $DL_DIR
        download.py --url $URL --file $FILE \
                    --sha 1b45f6aaa7f64486a4e6508caf0353141ca158b792702fd1c351189aad99b7da
    fi

    if [ ! -f $DIR/environment ]; then
        echo "extracting oss-cad-suite"
        mkdir -p $DIR
        tar xf $FILE -C $DIR --strip-components 1
    fi
    source $DIR/environment
}

setup_aarch64() {
    export PATH=$PATH:$REPO_TOP/boot/buildroot/build/output/host/bin/
    export CROSS_COMPILE=aarch64-none-linux-gnu-
    export ARCH=arm64
}
