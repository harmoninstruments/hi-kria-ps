#!/usr/bin/bash
source ../env.sh

FILE=$DL_DIR/picotool-$PICOTOOL_VER.tar.gz

echo "downloading and building picotool"
download.py \
    --url https://github.com/raspberrypi/picotool/archive/refs/tags/$PICOTOOL_VER.tar.gz \
    --file $FILE \
    --sha 2ed06b469913c86dea3d1e84d01e27c93853a4ebd65cebbefd2ad2c6d3e97780
tar xf $FILE
cd $PICOTOOL_PATH
mkdir -p build
cd build
cmake ..
make -j$(nproc)
