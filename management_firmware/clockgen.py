# Copyright (C) 2014-2022 Harmon Instruments, LLC
# SPDX-License-Identifier: MIT

import sys, struct, os
from amaranth.build import *
from amaranth.vendor.lattice_ice40 import *
from amaranth import *

class PWM_Delsig(Elaboratable):
    def __init__(self, ce, w, d, o):
        self.ce = ce # 10 MHz
        self.w = w # write
        self.d = d # DAC data
        self.o = o # 1 bit out
    def elaborate(self, platform):
        dq = Signal(len(self.d))
        pwm_count = Signal(4)
        on_prev = Signal()
        err = Signal(len(self.d) - 3)

        on = pwm_count != 0

        m = Module()
        m.d.sync += self.o.eq(~(on | (on_prev & err[-1])))
        m.d.sync += on_prev.eq(on)

        with m.If(self.w):
            m.d.sync += dq.eq(self.d)

        m.d.sync += pwm_count.eq(Mux(self.ce, dq[-4:], pwm_count - on))

        with m.If(self.ce):
            m.d.sync += err.eq(dq[:-4] + err[:-1])

        return m

class ClockGen(Elaboratable):
    def elaborate(self, platform):
        spi_sri = Signal(24)

        write_ocxo_tune = Signal()
        count = Signal(16)
        exref_stat = Signal()

        m = Module()
        m.domains.sync = ClockDomain(reset_less=True) # 100 MHz VCXO
        m.d.comb += ClockSignal().eq(platform.request("clock"))

        # binary count up
        m.d.sync += count.eq(count + 1)

        # clock div by 10
        count_10 = Signal(4)
        with m.Switch(count_10):
            for i in range(10):
                with m.Case(i):
                    m.d.sync += count_10.eq(i+2 if i == 6 else i+1)
            with m.Case():
                m.d.sync += count_10.eq(0)
        clk10 = Signal()
        m.d.sync += clk10.eq(count_10 < 5)
        ce_10 = Signal()
        m.d.sync += ce_10.eq(count_10 == 8)

        # clock outputs
        ps_clock = platform.request("ps_clock", 0, xdr=1)
        eth_clock = platform.request("eth_clock", 0, xdr=1)
        gtr_clock = platform.request("gtr_clock", 0, xdr=2)
        hpio_clock = platform.request("hpio_clock", 0, xdr=2)
        m.d.comb += [
            ps_clock.o_clk.eq(ClockSignal()),
            ps_clock.o.eq(count[0]),
            eth_clock.o_clk.eq(ClockSignal()),
            eth_clock.o.eq(count[1]),
            gtr_clock.o_clk.eq(ClockSignal()),
            gtr_clock.o0.eq(C(1,2)),
            gtr_clock.o1.eq(C(2,2)),
            hpio_clock.o_clk.eq(ClockSignal()),
            hpio_clock.o0.eq(C(1,2)),
            hpio_clock.o1.eq(C(2,2)),
        ]

        ref_out = platform.request("ref_out", 0, xdr=1)
        m.d.comb += [
            ref_out.o_clk.eq(ClockSignal()),
            ref_out.o.eq(count_10 > 4),
        ]

        ocxo_tune = platform.request("ocxo_tune", 0, xdr=1)
        m.submodules.pwm_ocxo = PWM_Delsig(ce = ce_10, w = write_ocxo_tune, d = spi_sri[8:], o = ocxo_tune.o)

        m.d.comb += [
            ocxo_tune.o_clk.eq(ClockSignal()),
        ]

        # obuf_reg sample_control_exref(.c(c), .i(count_10[3]), .o(exref_sample))

        exref = platform.request("exref")

        #m.submodules.freqdet_eref = FreqDet(i=exref, ce=ce_10, stat=exref_stat)

        return m

class ClockGenPlatform(LatticeICE40Platform):
    device      = "iCE40LP384"
    package     = "QN32"
    default_clk = "clock"
    resources = [
        Resource("clock", 0, Pins("6", dir='i'), Clock(100e6)), # VCXO
        Resource("exref", 0, Pins("8", dir='i'), Clock(10e6)), # 10 MHz from rear BNC
        Resource("spi", 0, # SPI from microcontroller not used in design
                 Subsignal("sck", Pins("15", dir="i")),
                 Subsignal("sdi", Pins("13", dir="i")),
        ),
        Resource("ps_clock", 0, Pins("31", dir="o")), # 50 MHz to Zynq PS
        Resource("eth_clock", 0, Pins("32", dir="o")), # 25 MHz to Ethernet
        Resource("ref_out", 0, Pins("7", dir="o")), # 10 MHz to BNC on rear via FF
        Resource("gtr_clock", 0, Pins("27 26", dir="o")), # 100 MHz to GTR
        Resource("hpio_clock", 0, Pins("29 30", dir="o")), # 100 MHz to HPIO
        Resource("ocxo_tune", 0, Pins("1", dir="o")), # PWM delta sigma
        Resource("pd", 0, Pins("20 19", dir="o")), # to ADC for exref phase detect
        Resource("zynq_ice", 0, Pins("14", dir="i")),
        Resource("ice_zynq", 0, Pins("12", dir="o")),
    ]
    connectors = []

if __name__ == "__main__":
    ClockGenPlatform().build(
        ClockGen(),
        name="clockgen_fpga",
        do_program=False,
        verbose=True,
        quiet=False,
    )

    # generate a header file with the bitstream data for inclusion in the
    # management microcontroller code
    with open('build/clockgen_fpga.bin', 'rb') as f:
        bs = f.read()
    # add 8 bytes (64 zeros, at least 49 required according to TN1248)
    # also, add sufficient bytes to make it a multiple of 4 bytes long
    bs += bytes(8 + 4 - (len(bs)&3))

    with open('bitstream.h', 'w') as f:
        print('static const uint32_t fpga_bitstream[] = {', file=f)
        for i in range(len(bs)//4):
            # Endian swap so that we can transmit it as 32 bit MSB first and have byte 0 first
            val, = struct.unpack(">I", bs[4*i:4*i+4])
            print(f'\t0x{val:08x},', file=f)
        print('};', file=f)
