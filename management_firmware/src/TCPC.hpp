// Harmon Instruments VNA
// Copyright (C) 2014 - 2022 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#pragma once

#include <cstdint>
#include <atomic>
#include "SPSC_FIFO.hpp"
#include "timer.hpp"

class tcpc_rxdata_str {
public:
        uint8_t len; // 3 + data bytes
        uint8_t type;
        uint16_t header;
        uint32_t data[7];
};

class USB_TCPC {
private:
        tcpc_rxdata_str rxdata;
        Timer timer_startup;
        Timer32 timer_adc;
        uint8_t message_id;
        uint8_t ccstat;
        uint8_t pwrstat;
        uint8_t faultstat;
        uint8_t alert_vd;
        uint8_t txfail = 0;
        uint8_t txsucc;
        uint8_t txdisc;
        uint8_t rxfull;
        int get_message_id() { return (message_id++) & 0x7; }
        void data_message(uint16_t header, const uint32_t *dos, int ndos);
        void control_message(uint16_t header) { data_message(header, NULL, 0); }
        void hard_reset();
        void handle_ccstat();
        void handle_capabilities();
        void handle_rx_packet();
        void interrupt();
        void request_pdo(int num);

public:
        SPSC_FIFO<tcpc_rxdata_str, 16> rx_fifo;
        USB_TCPC() {}
        // only written by TCPC thread, read anywhere
        struct tcpc_info_str {
                uint16_t vid;
                uint16_t pid;
                uint16_t devid;
                uint16_t typecrev;
                uint16_t usbpdrev;
                uint16_t pdifrev;
                uint32_t pdos[7];
                uint8_t n_pdos;
                uint8_t requested_pdo;
                uint16_t vbus_adc; // units of 0.025 V
                uint16_t state;
                uint8_t orient = 0;
                uint8_t pd_version;
        } info;

        struct Enable {
        public:
                bool en_5v : 1;
                bool en_1v8_5v5 : 1;
                bool en_n5v : 1;
                bool en_hv : 1;
                uint8_t as_uint() { return *reinterpret_cast<uint8_t *>(this); }
                bool operator==(Enable obj) {
                        return as_uint() == obj.as_uint();
                }
                bool operator!=(Enable obj) {
                        return as_uint() != obj.as_uint();
                };
        };

        static_assert(sizeof(Enable) == 1);

        // request set by the main thread
        std::atomic<Enable> enable_request;
        // set by the TCPC thread to ack
        std::atomic<Enable> enable_actual;

        void task();
};
