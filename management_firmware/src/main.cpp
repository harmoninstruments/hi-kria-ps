// Copyright (c) 2022 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "bsp/board.h"
#include "pico/stdlib.h"
#include "pico/multicore.h"
#include "pico/bootrom.h"
#include "hardware/uart.h"
#include "hardware/gpio.h"
#include "hardware/pwm.h"
#include "hardware/adc.h"
#include "hardware/structs/pads_qspi.h"
#include "hardware/structs/padsbank0.h"
#include "tusb.h"
#include "bitstream.h"

#include "sk6805.pio.h"
#include "spi32.pio.h"

#include "TCPC.hpp"
#include "GPIO.hpp"

USB_TCPC tcpc;

GPIOInput pbutton{0};
GPIOInput gpio_ld100{1};
GPIOOutput gpio_creset{5, false};
GPIOOutput gpio_le_pll{10, false};
GPIOOutputOD gpio_ps_por{13}; // SOM power on reset
GPIOOutputOD gpio_srst{14};   // SOM reset
GPIOOutputOD gpio_shutdown{23};
GPIOOutputOD gpio_mio_reset{26}; // USB, Enet PHYs
GPIOOutputOD gpio_poweroff{27};

// SOM boot mode control on GPIO 20-22
enum class BootMode : int { JTAG = 0, QSPI = 2, eMMC = 6, USB = 7 };
class BootModeSelect {
private:
        static constexpr uint32_t boot0pin = 20;
        static constexpr uint32_t bootmask = 0x7 << boot0pin;

public:
        BootModeSelect() {
                for (int i = 20; i < 23; i++) {
                        gpio_init(i);
                        gpio_disable_pulls(i);
                }
                gpio_clr_mask(7 << 20);
                set(BootMode::eMMC);
        }
        void set(const BootMode b) {
                uint32_t bootbits = static_cast<uint32_t>(b) ^ 0x7;
                gpio_set_dir_masked(bootmask, bootbits << boot0pin);
        }
} bootmode;

class Fan {
private:
        uint slice_num;
        uint pwm_ch;

public:
        Fan() {
                gpio_set_function(GPIO_FAN_PWM, GPIO_FUNC_PWM);
                slice_num = pwm_gpio_to_slice_num(GPIO_FAN_PWM);
                pwm_ch = pwm_gpio_to_channel(GPIO_FAN_PWM);
                pwm_set_wrap(slice_num, 5000); // 25 kHz from 125 MHz
                pwm_set_chan_level(slice_num, pwm_ch, 2500);
                pwm_set_enabled(slice_num, true);
                // fan tachometer
                gpio_set_pulls(GPIO_FAN_TACH, true, false); // pullup
                // static_assert(pwm_gpio_to_channel(GPIO_FAN_TACH) ==
                // PWM_CHAN_B);
                slice_num = pwm_gpio_to_slice_num(GPIO_FAN_TACH);
                // Count rising edges of PWM B
                pwm_config cfg = pwm_get_default_config();
                pwm_config_set_clkdiv_mode(&cfg, PWM_DIV_B_RISING);
                pwm_config_set_clkdiv(&cfg, 1);
                pwm_init(slice_num, &cfg, false);
                gpio_set_function(GPIO_FAN_TACH, GPIO_FUNC_PWM);
                pwm_set_enabled(slice_num, true);
                // return pwm_get_counter(slice_num);}
        }
        void set_speed(uint32_t pwmval) {
                pwm_set_chan_level(slice_num, pwm_ch, 2500);
        }
} fan;

void gpio_init() {
        pads_qspi_hw->voltage_select = 1;
        padsbank0_hw->voltage_select = 1;

        gpio_init_mask(GPIO_JTAG_MASK);
        gpio_clr_mask(GPIO_JTAG_MASK);
        gpio_set_dir_masked(GPIO_JTAG_MASK, GPIO_JTAG_OUT_MASK);

        adc_gpio_init(GPIO_IMON_VBUS);
        adc_gpio_init(GPIO_IMON_5V);
}

static void pll_spi(uint32_t x) {
        spi32_put(x);
        spi32_wait_idle(); // 10 ns last falling SCK to LE
        for (int i = 0; i < 4; i++)
                gpio_le_pll.set(); // 20 ns high minimum
        gpio_le_pll.clr();
}

// setup ADF4002, r = 1 for 10 MHz ref, 2 for 20 MHz
static void pll_init(int r = 1) {
        int n = 10;
        int cur = 4;    // 0 (0.6 mA) - 7 (5 mA)
        int muxout = 1; // 1 = digital lock detect, 3=Dvdd, 2=N, 4=R
        int pol = 1;    // 0: negative PFD polarity, 1 positive
        uint32_t func_init_latchval =
            (pol << 7) | (cur << 18) | (cur << 15) | (muxout << 4);
        pll_spi(3 | func_init_latchval); // initialization latch
        pll_spi(2 | func_init_latchval); // function latch
        pll_spi(0 | (r << 2));           // R
        pll_spi(1 | (n << 8));           // N
        spi32_put(0);                    // just to get COPI back to 0 state
}

// completes in about 3.74 ms
static void configure_ice40() {
        uint words_bs = sizeof(fpga_bitstream) / sizeof(fpga_bitstream[0]);
        for (uint i = 0; i < words_bs; i++)
                spi32_put(fpga_bitstream[i]);
}

enum class PowerState : int {
        Off,
        EnableSupplies,
        Startup1,
        Startup2,
        Startup3,
        Startup4,
        Startup5,
        On,
        Shutdown1,
};

class PowerControl {
protected:
        uint64_t t_last_led_update;
        uint32_t ledval = 0;
        uint64_t last_ts_power_not_pressed;
        uint64_t power_pressed_time;
        uint64_t ts_entering_state = 0;

public:
        void set_led(uint32_t x) { ledval = x; }
        void set_led(uint8_t r, uint8_t g, uint8_t b) {
                ledval = ((uint32_t)(r) << 8) | ((uint32_t)(g) << 16) |
                         (uint32_t)(b);
        }
        PowerState state{PowerState::Off};
        PowerControl() {
                t_last_led_update = time_us_32();
                // initialize PIO for LED
                PIO pio = pio0;
                int sm = 0;
                uint offset = pio_add_program(pio, &sk6805_program);
                sk6805_program_init(pio, sm, offset, GPIO_RGB);
                set_led(0xf, 0xf, 0x0);
        }
        void switch_state(PowerState x) {
                if (x == state)
                        return;
                ts_entering_state = time_us_64();
                state = x;
        }
        uint64_t time_in_state() { return time_us_64() - ts_entering_state; }
        void task();
} powercontrol;

void PowerControl::task() {
        if ((time_us_64() - t_last_led_update) > 100) {
                pio_sm_put_blocking(pio0, 0, ledval << 8u);
                t_last_led_update = time_us_64();
        }
        if (pbutton.val()) {
                power_pressed_time = 0;
                last_ts_power_not_pressed = time_us_64();
        } else {
                power_pressed_time = time_us_64() - last_ts_power_not_pressed;
        }
        switch (state) {
        case PowerState::Off:
                set_led(0xA, 0xA, 0x0);
                gpio_creset.clr();
                gpio_le_pll.clr();
                gpio_ps_por.clr();
                gpio_srst.set();
                gpio_shutdown.set();
                gpio_mio_reset.clr();
                gpio_poweroff.set();
                // after time for shutdown, disable all power, drop PD contract
                if (time_in_state() > 100000)
                        tcpc.enable_request = USB_TCPC::Enable{.en_5v = 0};
                // ignore the power button for 1 second
                if (time_in_state() < 1000000)
                        break;
                // start waking up if the power button is pressed
                if (power_pressed_time > 100000)
                        switch_state(PowerState::EnableSupplies);
                break;
        case PowerState::EnableSupplies:
                tcpc.enable_request = USB_TCPC::Enable{.en_5v = 1};
                if (tcpc.enable_actual.load() == tcpc.enable_request.load())
                        switch_state(PowerState::Startup1);
                break;
        case PowerState::Startup1:
                set_led(0x0, 0x0, 0xA);
                gpio_poweroff.clr();
                tcpc.enable_request =
                    USB_TCPC::Enable{.en_5v = 1, .en_1v8_5v5 = 1, .en_n5v = 1};
                if (time_in_state() > 50000)
                        switch_state(PowerState::Startup2);
                break;
        case PowerState::Startup2:
                set_led(0x0, 0x0, 0xA);
                gpio_creset.set();
                switch_state(PowerState::Startup3);
                break;
        case PowerState::Startup3:
                if (time_in_state() < 1000)
                        break;
                configure_ice40();
                pll_init(1); // attempt at 10 MHz
                switch_state(PowerState::Startup4);
                break;
        case PowerState::Startup4:
                if (time_in_state() < 150000)
                        break;
                if (gpio_ld100.val() != 0) // 10 MHz lock failed
                        pll_init(2);       // attempt at 20 MHz
                gpio_mio_reset.set();
                gpio_ps_por.set();
                switch_state(PowerState::Startup5);
                break;
        case PowerState::Startup5:
                if (power_pressed_time != 0)
                        break;
                if (time_in_state() > 500000) {
                        switch_state(PowerState::On);
                }
                break;
        case PowerState::On:
                set_led(0xA, 0x0, 0xA);
                if (time_in_state() < 1000000)
                        break;
                if (power_pressed_time > 100000)
                        switch_state(PowerState::Shutdown1);
                break;
        case PowerState::Shutdown1:
                gpio_mio_reset.clr();
                gpio_ps_por.clr();
                gpio_creset.clr();
                set_led(0xA, 0x0, 0xA);
                if (time_in_state() > 1000)
                        switch_state(PowerState::Off);
                break;
        }
}

// CDC0 is the console UART, UART1 on the RP2040, just passing data
static void console_task(void) {
        uint8_t buf[64];
        uint32_t count;
        if (tud_cdc_available()) {
                count = tud_cdc_read(buf, sizeof(buf));
                uart_write_blocking(uart1, buf, count);
                printf("got %d\n", count);
        }
        for (count = 0; count < sizeof(buf); count++) {
                if (!uart_is_readable(uart1))
                        break;
                tud_cdc_write_char(uart_getc(uart1));
        }
        if (count)
                tud_cdc_write_flush();
}

// do one bit, tms, tdo must be 0 or 1
// returns tdo
static inline int jtag_bit(int tms, int tdi) {
        gpio_put_masked(GPIO_JTAG_OUT_MASK,
                        (tms << GPIO_TMS) | (tdi << GPIO_TDI));
        gpio_put(GPIO_TCK, false);
        int rv = gpio_get(GPIO_TDO);
        // loop for delay, gives about 30 ns high
        for (int i = 0; i < 6; i++)
                gpio_put(GPIO_TCK, true);
        for (int i = 0; i < 6; i++)
                gpio_put(GPIO_TCK, false);
        return rv;
}

// bit bang JTAG
static uint8_t jtag_byte(uint8_t tms, uint8_t tdi, uint32_t bits) {
        uint8_t tdo = 0;
        uint8_t mask = 1;
        for (; bits; bits--) {
                gpio_put_masked(GPIO_JTAG_OUT_MASK,
                                (((tms & mask) != 0) << GPIO_TMS) |
                                    (((tdi & mask) != 0) << GPIO_TDI));
                if (gpio_get(GPIO_TDO))
                        tdo |= mask;
                // loop for delay, gives about 30 ns high
                for (int i = 0; i < 6; i++)
                        gpio_put(GPIO_TCK, true);
                for (int i = 0; i < 6; i++)
                        gpio_put(GPIO_TCK, false);
                if (mask == 0x80)
                        break;
                mask <<= 1;
        }
        return tdo;
}

static void usb_vendor_task(void) {
        if (!tud_vendor_available())
                return;
        uint32_t buf[16];
        uint32_t count = tud_vendor_read((uint8_t *)buf, sizeof(buf));
        // JTAG
        if ((buf[0] & 0xFFFF0000) == 0x94440000) {
                uint16_t low = buf[0] & 0xFFFF;
                int32_t bits = low & 0xFFF;
                if (bits > 240)
                        return;
                if (bits == 0)
                        return;
                uint32_t buffer_bytes = (bits + 7) >> 3;
                if (count < (buffer_bytes * 2 + 4))
                        return;
                uint8_t *tdo = reinterpret_cast<uint8_t *>(&buf[0]);
                uint8_t *tms = &tdo[4];
                uint8_t *tdi = &tms[buffer_bytes];
                for (uint32_t i = 0; bits > 0; bits -= 8) {
                        tdo[i] = jtag_byte(tms[i], tdi[i], bits);
                        i++;
                }
                // leave everything low in case power down is next
                gpio_clr_mask(GPIO_JTAG_MASK);
                if (low & 0x8000)
                        tud_vendor_write(tdo, buffer_bytes);
                return;
        }
        // JTAG memory write, starts, ends in RTI
        //[STATUS] SDR 35 TDI (A0000006) TDO (2) MASK (7)
        // 41 bits
        // 10000000000000000000000000000000000000110
        // 00001100000000000000000000000000101000000
        //      '00000000000000000000000000101000'
        // ---010-----------------------------------
        if ((buf[0] & 0xFFFF0000) == 0x94450000) {
                size_t words = buf[0] & 0xF;
                int inter_word_delay = 0xFF & (buf[0] >> 8);
                for (size_t i = 0; i < words; i++) {
                        // jtag_byte(0b000001, 0b110000, 6);
                        jtag_bit(1, 0); // to Select-DR
                        jtag_bit(0, 0); // to Capture-DR
                        jtag_bit(0, 0); // to Shift-DR
                        jtag_bit(0, 0); // in Shift-DR 0
                        jtag_bit(0, 1); // in Shift-DR 1
                        jtag_bit(0, 1); // in Shift-DR 1
                        uint32_t w = buf[i + 1];
                        for (size_t j = 0; j < 32; j++) {
                                jtag_bit(0, 1 & w);
                                w >>= 1;
                        }
                        /// jtag_byte(0b011, 0, 3);
                        jtag_bit(1, 0); // to Exit1-DR
                        jtag_bit(1, 0); // to Update-DR
                        jtag_bit(0, 0); // to RTI
                        for (int j = 0; j < inter_word_delay; j++)
                                gpio_put(GPIO_TCK, false);
                }
                return;
        }
        if (buf[0] == 0x286593a8) {
                bootmode.set(BootMode::eMMC);
                return;
        }
        if (buf[0] == 0x286593a9) {
                bootmode.set(BootMode::QSPI);
                return;
        }
        if (buf[0] == 0x286593aa) {
                bootmode.set(BootMode::JTAG);
                return;
        }
        if (buf[0] == 0xf8534353) {
                // fixme: wait for power down
                reset_usb_boot(0, 0); // reset to USB bootloader
                return;
        }
        if (buf[0] == 0xb12884fb) {
                // power down
                // fixme: wait for power down
                return;
        }
        if (buf[0] == 0xd19d94f2) { // get USB PD info
                static_assert(sizeof(USB_TCPC::info) <= 64);
                tud_vendor_write(reinterpret_cast<void *>(&tcpc.info),
                                 sizeof(tcpc.info));
                return;
        }
        if (buf[0] == 0xd19d94f3) { // get USB PD packet
                tcpc_rxdata_str rxdata{};
                rxdata.len = 255; // will get overwritten if FIFO not empty
                static_assert(sizeof(rxdata) <= 64);
                tcpc.rx_fifo.read(rxdata);
                tud_vendor_write(reinterpret_cast<void *>(&rxdata),
                                 sizeof(rxdata));
                return;
        }
        if (buf[0] == 0xe4e1f00a) { // get ADC readings
                uint16_t adcvals[3];
                adc_select_input(ADC_IMON_VBUS);
                adcvals[0] = adc_read();
                adc_select_input(ADC_IMON_5V);
                adcvals[1] = adc_read();
                adcvals[2] = tcpc.info.vbus_adc;
                tud_vendor_write(reinterpret_cast<void *>(adcvals),
                                 sizeof(adcvals));
                return;
        }
        if (buf[0] == 0) { // status struct
                buf[0] = 0xDEADBEEF;
                tud_vendor_write(buf, 64);
                return;
        }
}

void power_task() { tcpc.task(); }

int main(void) {
        board_init();
        tusb_init();
        // init UART1, the SOM console
        uart_init(uart1, 115200);
        uart_set_hw_flow(uart1, false, false);
        gpio_set_function(24, GPIO_FUNC_UART);
        gpio_set_function(25, GPIO_FUNC_UART);
        adc_init();
        gpio_init();
        spi32_init(GPIO_COPI, GPIO_SCK);
        multicore_launch_core1(power_task);
        while (1) {
                tud_task(); // tinyusb
                console_task();
                usb_vendor_task();
                powercontrol.task();
        }
        return 0;
}
