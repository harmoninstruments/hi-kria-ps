// Harmon Instruments VNA
// Copyright (C) 2014 - 2022 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#include <algorithm>
#include <stdio.h>

#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/i2c.h"

#include "TCPC.hpp"
#include "GPIO.hpp"

#define GPIO_SDA 2
#define GPIO_SCL 3
#define GPIO_INT 15

static const uint8_t phy_addr = 0x50;

GPIOInput gpio_int{GPIO_INT};

// Register addresses for TCPC
#define TCPC_ALERT 0x10
#define TCPC_ALERT_H 0x11
#define TCPC_STD_OUT_CFG 0x18
#define TCPC_CONTROL 0x19
#define TCPC_ROLECTL 0x1A
#define TCPC_POWERCTL 0x1C
#define TCPC_CCSTAT 0x1D
#define TCPC_PWRSTAT 0x1E
#define TCPC_FAULTSTAT 0x1F
#define TCPC_MSGHEADR 0x2E
#define TCPC_RXDETECT 0x2F
#define TCPC_TRANSMIT 0x50
#define TCPC_TRANSMIT_BYTE_COUNT 0x51
#define TCPC_TRANSMIT_HEADER 0x52
#define TCPC_TRANSMIT_DATA 0x54
#define TCPC_VBUS_ADC 0x70
#define TCPC_EXT_GPIO_CONFIG 0x92
#define TCPC_EXT_GPIO_CONTROL 0x93

// Bits in TCPC registers
#define TCPC_ALERT_CCSTAT (1 << 0)
#define TCPC_ALERT_PWRSTAT (1 << 1)
#define TCPC_ALERT_RXSTAT (1 << 2)
#define TCPC_ALERT_HARD_RESET (1 << 3)
#define TCPC_ALERT_TXFAIL (1 << 4)
#define TCPC_ALERT_TXDISC (1 << 5)
#define TCPC_ALERT_TXSUCC (1 << 6)
#define TCPC_CONTROL_ORIENT (1 << 0)
#define TCPC_CCSTAT_LOOK4CON (1 << 5)

static_assert(sizeof(USB_TCPC::info) == 48, "tcpc.info size");

static const int desired_pd_version = 2;
static const uint32_t i2c_timeout = 10000; // microseconds

// return the absolute time the I2C transaction should time out
static inline uint64_t get_timeout() { return i2c_timeout + time_us_64(); }

// write data to the TCPC
// addr is the register address in the TCPC not the I2C address
// returns number of bytes on success
static int i2c_write(uint8_t addr, const void *data, int count) {
        uint8_t tdata[257];
        tdata[0] = addr;
        if (count > 256)
                return PICO_ERROR_GENERIC;
        for (int i = 0; i < count; i++)
                tdata[i + 1] =
                    reinterpret_cast<uint8_t *>(const_cast<void *>(data))[i];
        int rv = i2c_write_blocking_until(i2c1, phy_addr, tdata, count + 1,
                                          false, get_timeout());
        return rv - 1; // subtracting address byte
}

// write a byte to a TCPC register
// returns 1 on success
static int i2c_write(uint8_t addr, uint8_t data) {
        return i2c_write(addr, &data, 1);
}

// write a 16 bit value to a TCPC register
// returns 2 on success
static int i2c_write_16(uint8_t addr, uint16_t data) {
        return i2c_write(addr, &data, 2);
}

// read data from the TCPC
// addr is the register address in the TCPC not the I2C address
// returns number of bytes on success
static int i2c_read(uint8_t addr, void *data, int count) {
        i2c_write_blocking_until(i2c1, phy_addr, &addr, sizeof(addr), true,
                                 get_timeout());
        return i2c_read_blocking_until(i2c1, phy_addr,
                                       reinterpret_cast<uint8_t *>(data), count,
                                       false, get_timeout());
}

static int i2c_read(uint8_t addr) {
        uint8_t rv;
        i2c_read(addr, &rv, 1);
        return rv;
}

static uint16_t i2c_read_16(uint8_t addr) {
        uint16_t rv;
        i2c_read(addr, &rv, 2);
        return rv;
}

// see enable_request in TCPC.hpp for bit defs
static void set_gpio(USB_TCPC::Enable val) {
        uint8_t gpioval = 0;
        // 5 V on FRS pin
        if (val.en_5v)
                gpioval |= (1 << 6);
        // 1.8 V on SRC pin
        if (val.en_1v8_5v5)
                gpioval |= (1 << 5);
        // -5 V on ILIM pin (inverted)
        if (!val.en_n5v)
                gpioval |= (1 << 3);
        i2c_write(TCPC_EXT_GPIO_CONTROL, gpioval);
}

void USB_TCPC::data_message(uint16_t header, const uint32_t *dos, int ndos) {
        header |=
            (info.pd_version << 6) | (get_message_id() << 9) | (ndos << 12);
        i2c_write(TCPC_TRANSMIT_BYTE_COUNT, 2 + ndos * 4);
        i2c_write_16(TCPC_TRANSMIT_HEADER, header);
        if (ndos > 0)
                i2c_write(TCPC_TRANSMIT_DATA, dos, ndos * 4);
        i2c_write(TCPC_TRANSMIT, (3 << 4));
}

void USB_TCPC::hard_reset() {
        i2c_write(TCPC_TRANSMIT, 0x05);
        // if RXDETECT is disabled, hard reset doesn't happen
}

void USB_TCPC::task() {
        // init RP2040 I2C
        i2c_init(i2c1, 100000);
        gpio_set_function(GPIO_SCL, GPIO_FUNC_I2C);
        gpio_set_function(GPIO_SDA, GPIO_FUNC_I2C);
        gpio_pull_up(GPIO_SCL);
        gpio_pull_up(GPIO_SDA);
        bi_decl(bi_2pins_with_func(GPIO_SCL, GPIO_SDA, GPIO_FUNC_I2C));
        info.pd_version = 2;
        // read the info registers
        uint16_t *p = reinterpret_cast<uint16_t *>(&info);
        for (int i = 0; i < 6; i++)
                p[i] = i2c_read_16(2 * i);
        // wait for the TCPC to complete initialization of the registers
        for (;;) {
                int pwrstat = i2c_read(TCPC_PWRSTAT);
                if (!(pwrstat & (1 << 6)))
                        break;
                sleep_ms(10);
        }
        // enable SRC (EN1.8), FRS (EN5), ILIM (~EN-5) as GPIO
        uint8_t tmp = i2c_read(TCPC_EXT_GPIO_CONFIG);
        i2c_write(TCPC_EXT_GPIO_CONFIG, tmp | (1 << 3) | (1 << 5) | (1 << 6));
        // set to default state
        i2c_write(TCPC_EXT_GPIO_CONTROL, 1 << 3);
        i2c_write(TCPC_CONTROL, TCPC_CONTROL_ORIENT);
        i2c_write(TCPC_MSGHEADR, info.pd_version << 1);
        i2c_write(TCPC_POWERCTL, 0x20); // enable the VBUS ADC
        i2c_write(TCPC_ROLECTL, 0xA);   // CC1 and CC2 set to Rd

        while (1) {
                if (gpio_int.val() == 0)
                        interrupt();
                if (info.state == 0) {
                        if (timer_startup.elapsed() > (5000 * TIMER_MS)) {
                                hard_reset();
                                timer_startup.reset();
                                // fixme: reset
                        }
                }
                if ((timer_startup.elapsed() > (4000 * TIMER_MS)) &&
                    (info.state == 2)) {
                        uint32_t vdos[4];
                        vdos[0] = 0xFF008001;
                        data_message(0xF, vdos, 1);
                        info.state = 3;
                }
                // vendor defined message (discover SVIDs)
                if ((timer_startup.elapsed() > 6000 * TIMER_MS) &&
                    (info.state == 3)) {
                        uint32_t vdos[4];
                        vdos[0] = 0xFF008002;
                        data_message(0xF, vdos, 1);
                        info.state = 4;
                }
                // Get source capabilities extended
                if ((timer_startup.elapsed() > 7000 * TIMER_MS) &&
                    (info.state == 4)) {
                        control_message(0x11);
                        info.state = 5;
                }
                // Get status
                if ((timer_startup.elapsed() > 8000 * TIMER_MS) &&
                    (info.state == 5)) {
                        control_message(0x12);
                        info.state = 6;
                }
                // update ADC reading
                if (timer_adc.elapsed() > 1000000) {
                        uint16_t adcval = i2c_read_16(TCPC_VBUS_ADC);
                        // compute voltage from PTN5110 register value, NXP
                        // AN12137 VBUS_VOLTAGE
                        adcval = (adcval & 0x3FF) << (adcval >> 10);
                        info.vbus_adc = adcval;
                        timer_adc.reset();
                }
                USB_TCPC::Enable tmp = enable_request;
                if (tmp != enable_actual) {
                        set_gpio(tmp);
                        enable_actual = tmp;
                }
        }
}

void USB_TCPC::handle_ccstat() {
        ccstat = i2c_read(TCPC_CCSTAT);
        // exit if it's still looking for a connection
        if (ccstat & TCPC_CCSTAT_LOOK4CON)
                return;
        // fixme: debounce?
        if ((ccstat & 0x3) != 0) { // use CC1
                i2c_write(TCPC_CONTROL, 0);
                info.orient = 1;
        } else {
                i2c_write(TCPC_CONTROL, TCPC_CONTROL_ORIENT);
                info.orient = 2;
        }

        sleep_ms(50);
        i2c_write(TCPC_MSGHEADR, info.pd_version << 1);
        i2c_write(TCPC_RXDETECT, 0x01); // receive detect, SOP only
}

void USB_TCPC::handle_rx_packet() {
        if (rxdata.len == 0)
                return;
        if (rxdata.type == 0) {
                bool extended = (rxdata.header & (1 << 15));
                int msg_type = rxdata.header & 0x1F;
                if (extended) {
                } else if (rxdata.len > 3) {
                        if (msg_type == 1) {
                                handle_capabilities();
                        } else if (msg_type == 0xF) { // VDM
                                // control_message_not_supported();
                        }
                } else {
                        // control message has no data
                        if (msg_type == 6) // PS_RDY
                                info.state = 2;
                }
        }
}

void USB_TCPC::request_pdo(int pdo) {
        uint32_t pro =
            ((pdo + 1) << 28) | (1 << 24) | (100 << 10) | (100 << 10);
        data_message(2, &pro, 1);
}

void USB_TCPC::handle_capabilities() {
        info.n_pdos = 0x7 & (rxdata.header >> 12);
        info.requested_pdo = 0;
        info.pd_version =
            std::min((rxdata.header >> 6) & 0x3, desired_pd_version);
        // int power_role = (rxdata.header & (1 << 8)) != 0;
        int voltage = 0;
        for (int i = 0; i < info.n_pdos; i++) {
                info.pdos[i] = rxdata.data[i];
                if ((info.pdos[i] >> 30) != 0)
                        continue;
                // find the highest voltage PDO that
                // LSB = 50 mV
                int v = (info.pdos[i] >> 10) & 0x3FF;
                // Limit to what we can safely accept, 21 volts
                const int v_21 = 21 / 0.05;
                if ((v > voltage) && (v < v_21))
                        info.requested_pdo = i;
        }
        request_pdo(info.requested_pdo);
        info.state = 1;
}

void USB_TCPC::interrupt() {
        uint16_t alert = i2c_read_16(TCPC_ALERT);

        if (alert & TCPC_ALERT_CCSTAT) {
                handle_ccstat();
                i2c_write(TCPC_ALERT, TCPC_ALERT_CCSTAT);
        }
        if (alert & TCPC_ALERT_PWRSTAT) {
                pwrstat = i2c_read(TCPC_PWRSTAT);
                i2c_write(TCPC_ALERT, TCPC_ALERT_PWRSTAT);
        }
        if (alert & TCPC_ALERT_RXSTAT) {
                i2c_read(0x30, &rxdata, 32);
                rx_fifo.write(rxdata);
                i2c_write(TCPC_ALERT, TCPC_ALERT_RXSTAT);
                handle_rx_packet();
        }
        if (alert & TCPC_ALERT_HARD_RESET) {
                i2c_write(TCPC_ALERT, TCPC_ALERT_HARD_RESET);
        }
        if (alert & TCPC_ALERT_TXFAIL) {
                i2c_write(TCPC_ALERT, TCPC_ALERT_TXFAIL);
                txfail++;
        }
        if (alert & TCPC_ALERT_TXDISC) {
                i2c_write(TCPC_ALERT, TCPC_ALERT_TXDISC);
                txdisc++;
        }
        if (alert & TCPC_ALERT_TXSUCC) {
                i2c_write(TCPC_ALERT, TCPC_ALERT_TXSUCC);
                txsucc++;
        }
        if (alert & (1 << 7)) {
                // high voltage alarm
                i2c_write(TCPC_ALERT, (1 << 7));
        }
        if (alert & (1 << 8)) {
                // low voltage alarm
                i2c_write_16(TCPC_ALERT, (1 << 8));
        }
        if (alert & (1 << 9)) {
                // read FAULTSTAT
                faultstat = i2c_read(TCPC_FAULTSTAT);
                i2c_write(TCPC_FAULTSTAT, 0xFF);
                i2c_write_16(TCPC_ALERT, (1 << 9));
        }
        if (alert & (1 << 10)) {
                // RX_FULL
                i2c_write_16(TCPC_ALERT, (1 << 10));
                rxfull++;
        }
        if (alert & (1 << 11)) {
                // sink disconnect detected
                i2c_write_16(TCPC_ALERT, (1 << 11));
        }
        if (alert & (1 << 15)) {
                // vendor defined
                i2c_write_16(TCPC_ALERT, (1 << 15));
        }
}
