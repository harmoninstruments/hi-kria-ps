// Harmon Instruments VNA
// Copyright (C) 2014 - 2022 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#pragma once

#include <cstdint>

static const uint32_t TIMER_MS = 1000;

class Timer {
private:
        uint64_t begin;

public:
        Timer() { reset(); }

        void reset() { begin = time_us_64(); }

        // return the elapsed time since reset in microseconds
        uint64_t elapsed() {
                uint64_t etime = time_us_64() - begin;
                return etime;
        }
};

class Timer32 {
private:
        uint32_t begin;

public:
        Timer32() { reset(); }

        void reset() { begin = time_us_32(); }

        // return the elapsed time since reset in microseconds
        uint32_t elapsed() {
                uint32_t etime = time_us_32() - begin;
                return etime;
        }
};
