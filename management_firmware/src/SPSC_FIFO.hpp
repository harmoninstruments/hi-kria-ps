// Harmon Instruments VNA
// Copyright (C) 2014 - 2022 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#pragma once
#include <cstdint>
#include <atomic>

// lock free single producer single consumer FIFO
// write should be called from a single execution context
// read should be called from a single execution context
// count may be called from either
// N must be a power of 2
template <class T, size_t N> class SPSC_FIFO {
private:
        static_assert(N <= 256);
        std::atomic<uint8_t> pin = 0;
        std::atomic<uint8_t> pout = 0;
        T data[N];

public:
        int count() { return (N - 1) & (pin - pout); }
        int write(const T &d) {
                if (count() == N - 1)
                        return -1;
                data[pin] = d;
                pin = (pin + 1) & (N - 1);
                return 0;
        }
        int read(T &d) {
                if (count() == 0)
                        return -1;
                d = data[pout];
                pout = (pout + 1) & (N - 1);
                return 0;
        }
};
