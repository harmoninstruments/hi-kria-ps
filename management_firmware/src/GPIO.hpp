// Copyright (c) 2022 Harmon Instruments, LLC
// SPDX-License-Identifier: MIT

#pragma once

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "bsp/board.h"
#include "pico/stdlib.h"
#include "hardware/gpio.h"
#include "hardware/structs/pads_qspi.h"
#include "hardware/structs/padsbank0.h"

class GPIOOutput {
protected:
        const uint32_t pin;

public:
        GPIOOutput(uint32_t pin, bool init = false) : pin(pin) {
                gpio_init(pin);
                put(init);
                gpio_set_dir_masked(1 << pin, 1 << pin);
        }
        void put(bool x) { gpio_put(pin, x); }
        void clr() { gpio_clr_mask(1 << pin); }
        void set() { gpio_set_mask(1 << pin); }
        bool val() { return gpio_get(pin); }
};

class GPIOOutputOD {
protected:
        const uint32_t pin;

public:
        GPIOOutputOD(uint32_t pin) : pin(pin) {
                gpio_init(pin);
                gpio_clr_mask(1 << pin);
                gpio_disable_pulls(pin);
                gpio_set_dir_in_masked(1 << pin);
        }
        void clr() { gpio_set_dir_out_masked(1 << pin); }
        void set() { gpio_set_dir_in_masked(1 << pin); }
        bool val() { return gpio_get(pin); }
};

class GPIOInput {
protected:
        const uint32_t pin;

public:
        GPIOInput(uint32_t pin) : pin(pin) {
                gpio_init(pin);
                gpio_pull_up(pin);
        }
        bool val() { return gpio_get(pin); }
};

#define GPIO_RGB 4
#define GPIO_COPI 6
#define GPIO_SCK 7
#define GPIO_FAN_TACH 11
#define GPIO_FAN_PWM 12
#define GPIO_PSU_INT 15
#define GPIO_TMS 16
#define GPIO_TDO 17
#define GPIO_TDI 18
#define GPIO_TCK 19
#define GPIO_JTAG_MASK (0xF << 16)
#define GPIO_JTAG_OUT_MASK ((1 << GPIO_TMS) | (1 << GPIO_TDI) | (1 << GPIO_TCK))
#define GPIO_IMON_VBUS 28
#define GPIO_IMON_5V 29

#define ADC_0_GPIO 26
#define ADC_IMON_VBUS (GPIO_IMON_VBUS - ADC_0_GPIO)
#define ADC_IMON_5V (GPIO_IMON_5V - ADC_0_GPIO)
