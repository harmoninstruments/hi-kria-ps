#!/usr/bin/bash
source ../env.sh
setup_arm32
setup_oss-cad-suite

FILE=$DL_DIR/pico-sdk-$PICO_SDK_VER.tar.gz

TINYUSB_VER=0.13.0
TINYUSB_FILE=$DL_DIR/tinyusb-$TINYUSB_VER.tar.gz

if [[ -d $PICO_SDK_PATH ]]
then
    echo "using existing pico-sdk"
else
    echo "downloading pico-sdk"
    download.py \
    --url https://github.com/raspberrypi/pico-sdk/archive/refs/tags/$PICO_SDK_VER.tar.gz \
    --file $FILE \
    --sha 0070d562b1767ccd9ceca4f7f2eaef48f06029950604a43c510247b8eeebf329
    tar xf $FILE

    download.py \
    --url https://github.com/hathach/tinyusb/archive/refs/tags/$TINYUSB_VER.tar.gz \
    --file $TINYUSB_FILE \
    --sha 9eec576880be28d8a05b32b470a67e1a095ceb9d221a8a345e420ef61c365c91
    tar xf $TINYUSB_FILE -C $PICO_SDK_PATH/lib/tinyusb --strip-components 1
fi

mkdir -p build
cd build
cmake ..
make -j$(nproc)
